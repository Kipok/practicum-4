#include <iostream>
#include <cstdio>
#include <stdexcept>
#include <vector>
#include <utility>
using namespace std;

namespace Game
{
    template <typename T>
    class Coord
    {
        T row;
        T col;
    public:
        typedef T value_type;
        Coord() : row(), col() {}
        Coord(const T &row_, const T &col_) : row(row_), col(col_) {}
        Coord(const Coord & crd) : row(crd.row), col(crd.col) {}
        bool operator ==(const Coord &crd) const
        {
            return row == crd.row && col == crd.col;
        }
        bool operator != (const Coord &crd) const
        {
            return !(*this == crd);
        }
        T get_row() const
        {
            return row;
        }
        T get_col() const
        {
            return col;
        }
        friend ostream & operator <<(ostream &out, const Coord &crd)
        {
            out << '(' << crd.row << ',' << crd.col << ')';
            return out;
        }
        bool operator <(const Coord &crd) const
        {
            return row < crd.row || (row == crd.row && col < crd.col);
        }
        bool operator >(const Coord &crd) const
        {
            return row > crd.row || (row == crd.row && col > crd.col);
        }
        bool operator <=(const Coord &crd) const
        {
            return !(*this > crd);
        }
        bool operator >=(const Coord &crd) const
        {
            return !(*this < crd);
        }
    };
    typedef Coord<int> IntCoord;
}