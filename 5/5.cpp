#include <iostream>

template<typename T>
class HasX
{
    struct Fallback { int x; };
    struct Derived : T, Fallback { };

    template<typename U, U> struct Check;

    typedef char ArrayOfOne[1];
    typedef char ArrayOfTwo[2];

    template<typename U>
    static ArrayOfOne & func(Check<int Fallback::*, &U::x> *);

    template<typename U>
    static ArrayOfTwo & func(...);

public:
    static constexpr bool value = sizeof(func<Derived>(0)) == 2;
};