#include <iostream>

using namespace std;

struct X;
void f(X &x, int n);
const int P = 1;
const int Q = 1;
const int R = 1;

struct X
{
    X()
    {
        try {
            f(*this, -1);
            cout << 'd' << endl;
        }
        catch (X) {
            cout << 11 << endl;
        }
        catch (int) {
            cout << 'f' << endl;
        }
    }
    X(X &x)
    {
        cout << 2 << endl;
    }
    virtual ~X()
    {
        cout << 13 << endl;
    }
};

struct Y : public X
{
    Y()
    {
        f(*this, 1);
        cout << 6 << endl;
    }
    Y(Y& y)
    {
        cout << 'b' << endl;
    }
    ~Y()
    {
        cout << 4 << endl;
    }
};

void f(X &x, int n)
{
    try {
        if (n < 0) throw x;
        if (n > 0) throw 1;
        cout << 14 << endl;
    }
    catch (int) {
        cout << 7 << endl;
    }
    catch (X& a) {
        cout << 3 << endl;
        f(a, 0);
        cout << 5 << endl;
        throw;
    }
}

int main()
{
    try {
        Y a;
    }
    catch (...) {
        cout << 'g' << endl;
        return 0;
    }
    cout << 10 << endl;
    return 0;
}