#include <iostream>
#include <cstdio>
#include <stdexcept>
#include <vector>
#include <utility>
using namespace std;

const int dx1[6] = { 0, -1, 1, -1, 0, 1 };
const int dy1[6] = { -1, 0, 0, 1, 1, 1 };
const int dx2[6] = { -1, 0, 1, -1, 1, 0 };
const int dy2[6] = { -1, -1, -1, 0, 0, 1 };


template <typename T>
class HexTopology
{
    const int dx1[6] = { 0, -1, 1, -1, 0, 1 };
    const int dy1[6] = { -1, 0, 0, 1, 1, 1 };
    const int dx2[6] = { -1, 0, 1, -1, 1, 0 };
    const int dy2[6] = { -1, -1, -1, 0, 0, 1 };
    int row_count, col_count;
public:
    HexTopology(const T &m) : row_count(0), col_count(0)
    {
        row_count = m.get_rows();
        col_count = m.get_cols();
    }
    HexTopology(int row_count_, int col_count_) : row_count(row_count_), col_count(col_count_)
    {
        if (row_count < 0 || col_count < 0) {
            throw range_error("");
        }
    }
    vector<pair<int, int> > operator() (int row, int col) const
    {
        if (row < 0 || col < 0 || row >= row_count || col >= col_count) {
            throw range_error("");
        }
        vector <pair <int, int > > v;
        for (int i = 0; i < 6; i++) {
            if (col % 2 == 0) {
                if (row + dy1[i] >= 0 && row + dy1[i] < row_count && col + dx1[i] >= 0 && col + dx1[i] < col_count) {
                    v.push_back(make_pair(row + dy1[i], col + dx1[i]));
                }
            }
            else {
                if (row + dy2[i] >= 0 && row + dy2[i] < row_count && col + dx2[i] >= 0 && col + dx2[i] < col_count) {
                    v.push_back(make_pair(row + dy2[i], col + dx2[i]));
                }
            }
        }
        return v;
    }
};