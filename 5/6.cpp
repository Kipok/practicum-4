#include <iostream>
#include <cstdio>
#include <stdexcept>
#include <string>
using namespace std;

template <class T>
class PointerMatrix
{
    class ptr
    {
        int idx;
        PointerMatrix *mat;
    public:
        ptr& operator =(T * obj) {
            if (mat->m[idx] == obj) {
                return *this;
            }
            delete mat->m[idx];
            mat->m[idx] = obj;
            return *this;
        }
        T* operator ->() const {
            return mat->n[idx];
        }
        operator T*() const {
            return mat->m[idx];
        }
    public:
        ptr(int idx_, PointerMatrix *mat_) : idx(idx_), mat(mat_) {}
    };
    int rows;
    int cols;
    T* *m;
    PointerMatrix(const PointerMatrix &) = delete;
    PointerMatrix & operator=(const PointerMatrix &) = delete;
public:
    const static int ROWS_MAX = 16384;
    const static int COLS_MAX = 16384;
    PointerMatrix(int r, int c, T *obj) : rows(r), cols(c), m(nullptr)
    {
        if (rows >= ROWS_MAX || cols >= COLS_MAX || rows <= 0 || cols <= 0) {
            throw invalid_argument("Wrong matrix size");
        }
        m = new T*[r * c];
        for (int i = 0; i < r; i++) {
            for (int j = 0; j < c; j++) {
                m[i * c + j] = obj->clone();
            }
        }
    }
    ~PointerMatrix()
    {
        for (int i = 0; i < rows; i++) {
            for (int j = 0; j < cols; j++) {
                delete m[i * cols + j];
            }
        }
        delete m;
    }
    inline int get_rows() const
    {
        return rows;
    }
    inline int get_cols() const
    {
        return cols;
    }
    ptr at(int r, int c)
    {
        if (r >= rows || c >= cols || r < 0 || c < 0) {
            throw range_error("Wrong matrix size");
        }
        return ptr(r * cols + c, this);
    }
    T * at(int r, int c) const
    {
        if (r >= rows || c >= cols || r < 0 || c < 0) {
            throw range_error("Wrong matrix size");
        }
        return m[r * cols + c];
    }
};