#include <iostream>
#include <cstdio>
using namespace std;

class ex
{
    int num;
    int fl;
public:
    ex(int num_, int fl_) : num(num_), fl(fl_) {}
    void change_num(int num_) {
        num = num_;
    }
    ~ex() {
        if (fl) {
            cout << num << ' ';
        }
    }
};

void f()
{
    int num = 0;
    try {
        cin >> num;
        if (cin.fail()) {
            throw 1;
        }
        f();
    }
    catch (ex &e) {
        throw ex(num, 1);
    }
    catch (int) {
        throw ex(num, 0);
    }
}

int main()
{
    try {
        f();
    }
    catch (ex &e) {

    }
    return 0;
}