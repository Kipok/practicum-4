#include <iostream>
#include <cstdio>

using namespace std;

template <unsigned int n>
struct Fib
{
    static const unsigned VAL = Fib<n - 1>::VAL + Fib<n - 2>::VAL;
};

template <>
struct Fib <0>
{
    static const unsigned VAL = 0;
};

template <>
struct Fib <1>
{
    static const unsigned VAL = 1;
};