#include <iostream>
#include <cstdio>
using namespace std;

template<typename T>
class IsClass
{
    typedef char YES[1];
    typedef char NO[2];

    template<typename U>
    static YES & func(int U::* *);

    template<typename U>
    static NO & func(...);

public:
    static const bool value = sizeof(func<T>(0)) == 1;
};