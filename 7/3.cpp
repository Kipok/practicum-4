#include <iostream>
#include <cstdio>
#include <string>
using namespace std;

bool belongs(char c, const string &s_alph)
{
    for (unsigned int i = 0; i < s_alph.size(); i++) {
        if (c == s_alph[i]) {
            return true;
        }
    }
    return false;
}

void gen(int n, int k, string &s, const string &f_alph, const string &s_alph)
{
    if (n == k) {
        cout << s << endl;
        return;
    }
    for (unsigned int i = 0; i < s_alph.size(); i++) {
        s[n] = s_alph[i];
        gen(n + 1, k, s, f_alph, s_alph);
    }
    if (n == 0 || (n > 0 && belongs(s[n - 1], f_alph))) {
        for (unsigned int i = 0; i < f_alph.size(); i++) {
            s[n] = f_alph[i];
            gen(n + 1, k, s, f_alph, s_alph);
        }
    }
}

void get_chains(int k, const string &f_alph, const string &s_alph)
{
    string s(k, '1');
    gen(0, k, s, f_alph, s_alph);
}

int main(void)
{
    int k;
    cin >> k;
    const string f_alph = "34";
    const string s_alph = "12";
    get_chains(k, f_alph, s_alph);
    return 0;
}