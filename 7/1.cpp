#include <iostream>
#include <cstdio>
#include <string>
#include <algorithm>
using namespace std;

inline bool isTerminal(char c)
{
    return c >= 'a' && c <= 'z';
}

inline bool isNonTerminal(char c)
{
    return c >= 'A' && c <= 'Z';
}

bool isRightRegular(const string &s1, const string &s2)
{
    if (s1.size() != 1 || !isNonTerminal(s1[0])) {
        return false;
    }
    for (unsigned int i = 0; i < s2.size() - 1; i++) {
        if (!isTerminal(s2[i])) {
            return false;
        }
    }
    return true;
}

bool isLeftRegular(const string &s1, const string &s2)
{
    if (s1.size() != 1 || !isNonTerminal(s1[0])) {
        return false;
    }
    for (unsigned int i = 1; i < s2.size(); i++) {
        if (!isTerminal(s2[i])) {
            return false;
        }
    }
    return true;
}

bool isContextFree(const string &s1, const string &s2)
{
    if (s1.size() != 1 || !isNonTerminal(s1[0])) {
        return false;
    }
    return true;
}

bool checkValid(int pos, const string &s1, const string &s2)
{
    for (int i = 0; i < pos; i++) {
        if (s1[i] != s2[i]) {
            return false;
        }
    }
    int j = s2.size() - 1;
    for (int i = s1.size() - 1; i > pos; i--) {
        if (s1[i] != s2[j--]) {
            return false;
        }
    }
    if (j < pos) {
        return false;
    }
    return true;
}

bool isContextSensitive(const string &s1, const string &s2)
{
    if (s1.size() == 0) {
        return false;
    }
    for (unsigned int i = 0; i < s1.size(); i++) {
        if (isNonTerminal(s1[i])) {
            if (checkValid(i, s1, s2)) {
                return true;
            }
        }
    }
    return false;
}

bool isNonShortened(const string &s1, const string &s2)
{
    return s1.size() <= s2.size();
}

enum GType {
    NOTHING = 0,
    NON_SHORTENED = 1,
    CONTEXT_SENSITIVE = 2,
    CONTEXT_FREE = 3,
    RIGHT_REGULAR = 4,
    LEFT_REGULAR = 5
};

int get_type(const string &s1, const string &s2)
{
    if (isLeftRegular(s1, s2)) {
        return LEFT_REGULAR;
    }
    if (isRightRegular(s1, s2)) {
        return RIGHT_REGULAR;
    }
    if (isContextFree(s1, s2)) {
        return CONTEXT_FREE;
    }
    if (isContextSensitive(s1, s2)) {
        return CONTEXT_SENSITIVE;
    }
    if (isNonShortened(s1, s2)) {
        return NON_SHORTENED;
    }
    return NOTHING;
}

void print_type(int type)
{
    switch (type) {
    case LEFT_REGULAR:
        cout << "3 left-regular\n";
        break;
    case RIGHT_REGULAR:
        cout << "3 right-regular\n";
        break;
    case CONTEXT_SENSITIVE:
        cout << "1 context-sensitive\n";
        break;
    case NON_SHORTENED:
        cout << "1 non-shortened\n";
        break;
    case CONTEXT_FREE:
        cout << "2\n";
        break;
    default:
        cout << "0\n";
        break;
    }
}

int main(void)
{
    string s1, s2;
    int type = LEFT_REGULAR;
    bool s_flag = false, se_flag = false, was_l = false, was_empty = false;
    while (cin >> s1 >> s2) {
        if (s2.find('S') != string::npos) {
            s_flag = true;
        }
        if (s1 == "S" && s2 == "_") {
            se_flag = true;
            continue;
        }
        if (s1 != "S" && s2 == "_") {
            was_empty = true;
        }
        int g_t = get_type(s1, s2);
        type = min(type, g_t);
        if (g_t == LEFT_REGULAR && !isRightRegular(s1, s2)) {
            was_l = true;
        }
    }
    if (type == RIGHT_REGULAR && was_l == true) {
        type = CONTEXT_FREE;
    }
    if ((type == CONTEXT_SENSITIVE || type == NON_SHORTENED) && 
    (was_empty == true || (se_flag == true && s_flag == true))) {
        type = NOTHING;
    }
    print_type(type);
    return 0;
}