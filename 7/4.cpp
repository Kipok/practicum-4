#include <iostream>
#include <cstdio>
#include <string>
#include <iterator>
#include <algorithm>
#include <vector>
using namespace std;

bool belongs(char c, const string &s_alph)
{
    for (unsigned int i = 0; i < s_alph.size(); i++) {
        if (c == s_alph[i]) {
            return true;
        }
    }
    return false;
}

void gen(int n, int k, string &s, const string &f_alph, const string &s_alph, vector <string> &ans)
{
    if (n == k) {
        unsigned int i;
        for (i = 0; i < s.size(); i++) {
            if (!belongs(s[i], f_alph)) {
                break;
            }
        }
        string to_push(s.size() * 2, '0');
        copy(s.begin(), s.begin() + i, to_push.begin());
        copy(s.rbegin() + k - i, s.rend(), to_push.begin() + i);
        copy(s.begin() + i, s.end(), to_push.begin() + 2 * i);
        copy(s.rbegin(), s.rbegin() + k - i, to_push.begin() + to_push.size() - k + i);
        ans.push_back(to_push);
        return;
    }
    for (unsigned int i = 0; i < s_alph.size(); i++) {
        s[n] = s_alph[i];
        gen(n + 1, k, s, f_alph, s_alph, ans);
    }
    if (n == 0 || (n > 0 && belongs(s[n - 1], f_alph))) {
        for (unsigned int i = 0; i < f_alph.size(); i++) {
            s[n] = f_alph[i];
            gen(n + 1, k, s, f_alph, s_alph, ans);
        }
    }
}

void get_chains(int k, const string &f_alph, const string &s_alph, vector <string> &ans)
{
    string s(k / 2, '1');
    gen(0, k / 2, s, f_alph, s_alph, ans);
}

int main(void)
{
    int k;
    cin >> k;
    const string f_alph = "34";
    const string s_alph = "12";
    vector <string> ans;
    get_chains(k, f_alph, s_alph, ans);
    sort(ans.begin(), ans.end());
    copy(ans.begin(), ans.end(), ostream_iterator<string>(cout, "\n"));
    return 0;
}