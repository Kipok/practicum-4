#include <iostream>
#include <cstdio>
#include <stdexcept>
#include <string>
#include <fstream>
#include <vector>
using namespace std;

enum {
    LANG_1,
    LANG_2,
    NOT_VALID,
    END
};

bool check(char c, int &mode)
{
    if (mode == LANG_1) {
        if (c == '3' || c == '4') {
            return true;
        }
        if (c == '1' || c == '2') {
            mode = LANG_2;
            return true;
        }
        mode = NOT_VALID;
        return false;
    }
    if (mode == LANG_2) {
        if (c == '1' || c == '2') {
            return true;
        }
        mode = END;
        return true;
    }
    mode = NOT_VALID;
    return false;
}

int main()
{
    string s;
    while (cin >> s) {
        int sign = LANG_1;
        unsigned int i;
        for (i = 0; i < s.size(); i++) {
            if (check(s[i], sign) == false) {
                cout << 0 << endl;
                break;
            }
            if (sign == END || sign == NOT_VALID) {
                cout << 0 << endl;
                break;
            }
        }
        if (i == s.size()) {
            cout << 1 << endl;
        }
    }
    return 0;
}