#include <iostream>
#include <cstdio>
#include <cstdlib>
#include <string>
#include <ctime>
#include <vector>
#include <algorithm>
#include <functional>
#include <numeric>
using namespace std;

class game
{
    vector <int> ans;
    int num_pos, num_col;
    vector <int> guess;
public:
    game(const int n_pos, const int n_col) : ans(), num_pos(n_pos), num_col(n_col), guess()
    {
        srand(time(NULL));
        ans.resize(num_pos);
        guess.resize(num_pos);
        generate(ans.begin(), ans.end(), [n_col](){ return 1 + rand() % n_col; });
    }
    void start_game()
    {
        cout << "Well, your turn :)\n";
        find_if(istream_iterator<string>(cin), istream_iterator<string>(), bind1st(mem_fun(&game::get_turn), this));
    }
    bool get_turn(string s)
    {
        get_guess(s);
        int b_hits = 0, w_hits = 0;
        vector <int> num_ans(num_col, 0), num_guess(num_col, 0);
        vector <int> new_ans(num_pos), new_guess(num_pos);
        transform(ans.begin(), ans.end(), guess.begin(), new_ans.begin(), [](int i1, int i2) { return i1 == i2 ? 0 : i1; });
        transform(ans.begin(), ans.end(), guess.begin(), new_guess.begin(), [](int i1, int i2) { return i1 == i2 ? 0 : i2; });
        b_hits = accumulate(new_ans.begin(), new_ans.end(), 0, [](int init, int cur) { return init + !cur; });

        sort(new_ans.begin(), new_ans.end());
        new_ans.erase(remove_if(new_ans.begin(), new_ans.end(), [](int n){ return n == 0; }), new_ans.end());
        sort(new_guess.begin(), new_guess.end());
        new_guess.erase(remove_if(new_guess.begin(), new_guess.end(), [](int n){ return n == 0; }), new_guess.end());
        vector <int> inter(num_pos);
        auto it = set_intersection(new_ans.begin(), new_ans.end(), new_guess.begin(), new_guess.end(), inter.begin());
        w_hits = distance(inter.begin(), it);

        cout << "Black hits: " << b_hits << endl << "White hits: " << w_hits << endl;
        if (b_hits == num_pos) {
            cout << "Congratulations!\n";
            return true;
        }
        else {
            return false;
        }
    }
    void get_guess(string g)
    {
        transform(g.begin(), g.end(), guess.begin(), [](int i) { return i - '0'; });
    }
};


int main(void)
{
    game gm(4, 6);
    gm.start_game();
    return 0;
}