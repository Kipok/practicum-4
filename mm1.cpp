#include <iostream>
#include <cstdio>
#include <cstdlib>
#include <string>
#include <ctime>
using namespace std;

class guess
{
    string msg;
public:
    guess() : msg("") {}
    void get_guess() 
    {
        cin >> msg;
    }
    char operator [](const int pos) const 
    {
        return msg[pos];
    }
};

class game
{
    string ans;
    int num_pos, num_col;
    guess g;
public:
    game(const int n_pos, const int n_col) : num_pos(n_pos), num_col(n_col), ans("")
    {
        srand(time(NULL));
        for (int i = 0; i < num_pos; i++) {
            ans.push_back('1' + (rand() % num_col));
        }
    }
    game(const int n_pos, const int n_col, string ans_) : num_pos(n_pos), num_col(n_col), ans(ans_) {}
    void start_game()
    {
        cout << "Well, your turn :)\n";
        g.get_guess();
    }
    void get_turn() const 
    {
        int b_hits = 0, w_hits = 0;
        string u = ans;
        for (int i = 0; i < num_pos; i++) {
            if (g[i] == ans[i]) {
                u[i] = '0';
                ++b_hits;
            } else {
                for (int j = 0; j < num_pos; j++) {
                    if (g[i] == u[j]) {
                        ++w_hits;
                        u[j] = '0';
                    }
                }
            }
        }
        cout << "Black hits: " << b_hits << endl << "White hits: " << w_hits << endl;
        if (b_hits == num_pos) {
            cout << "Congratulations!\n";
            return false;
        }
        return true;        
    }
    void get_guess() 
    {
        g.get_guess();
    }
};

int main(void)
{
    game gm(4, 6);
    gm.start_game();
    while (gm.get_turn()) {
        gm.get_guess();
    }
    return 0;
}

