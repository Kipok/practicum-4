#include <iostream>
#include <cstdio>
#include <vector>
#include <string>
#include <ctime>

typedef unsigned int uint;
typedef uint h_type;
typedef uint d_type;
typedef uint amm_type;
#define inf -1

enum town_type {
    CASTLE = 1,
    TOWER = 2
};

enum unit_type {
    SHOOTING = 1,
    PRANCING = 2,
    MAGICIAN = 3
};

enum attack_type {
    DISTANCE,
    CLOSE
};

class Unit
{
protected:
    struct UnitParams {
        std::string name;
        h_type health;
        attack_type attack;
        amm_type ammunition;
        UnitParams(std::string n, h_type h, attack_type a, amm_type amm) : 
            name(n), health(h), attack(a), ammunition(amm) {}
    };
    UnitParams pm;
    virtual inline d_type GetDamage() const //why can't make it pure?
    {
        return 10;
    }
public:
    Unit(UnitParams pm_) : pm(pm_) {}
    virtual ~Unit() {}
    h_type GetHealth() const
    {
        return pm.health;
    }
    virtual d_type Attack()
    {
        return GetDamage();
    }
    std::string ToString() const 
    {
        return pm.name;
    }
};

class Shooting : public Unit
{
public:
    Shooting(UnitParams pm_) : Unit(pm_) {}
    virtual ~Shooting() {}
};

class Prancing : public Unit
{
public:
    Prancing(UnitParams pm_) : Unit(pm_) {}
    virtual ~Prancing() {}
};

class Magician : public Unit
{
public:
    Magician(UnitParams pm_) : Unit(pm_) {}
    virtual ~Magician() {}
};

class Archery : public Shooting
{
    virtual inline d_type GetDamage() const
    {
        return 10;
    }
public:
    Archery() : Shooting(UnitParams("Archery", 45, DISTANCE, 15)) {}
    virtual ~Archery() {}
    virtual d_type Attack()
    {
        if (pm.ammunition == 1) {
            pm.ammunition = 0;
            return GetDamage();
        }
        if (pm.ammunition == 0) {
            return 0;
        }
        pm.ammunition -= 2;
        return GetDamage() * 2;
    }
};

class Knight : public Prancing
{
    virtual inline d_type GetDamage() const
    {
        return 20;
    }
public:
    Knight() : Prancing(UnitParams("Knight", 145, CLOSE, inf)) {}
    virtual ~Knight() {}
};

class Monk : public Magician
{
    virtual inline d_type GetDamage() const
    {
        return 20 + (rand() % 11);
    }
public:
    Monk() : Magician(UnitParams("Monk", 100, DISTANCE, 30)) {}
    virtual ~Monk() {}
    virtual d_type Attack()
    {
        if (pm.ammunition == 0) {
            return 0;
        }
        d_type d_tmp = GetDamage();
        pm.ammunition = (pm.ammunition > d_tmp / 10) ? (pm.ammunition - d_tmp / 10) : 0;
        return d_tmp;
    }
};

class MasterGremlin : public Shooting
{
    virtual inline d_type GetDamage() const
    {
        return 15;
    }
public:
    MasterGremlin() : Shooting(UnitParams("MasterGremlin", 25, DISTANCE, 10)) {}
    virtual ~MasterGremlin() {}
    virtual d_type Attack()
    {
        if (pm.ammunition == 0) {
            return 0;
        }
        --pm.ammunition;
        return GetDamage() + pm.ammunition / 2;
    }

};

class Golem : public Prancing
{
    virtual inline d_type GetDamage() const
    {
        return 30;
    }
public:
    Golem() : Prancing(UnitParams("Golem", 175, CLOSE, inf)) {}
    virtual ~Golem() {}
};

class Wizard : public Magician
{
    virtual inline d_type GetDamage() const
    {
        return 30 + (rand() % 21);
    }
public:
    Wizard() : Magician(UnitParams("Wizard", 90, DISTANCE, 50)) {}
    virtual ~Wizard() {}
    virtual d_type Attack()
    {
        if (pm.ammunition == 0) {
            return 0;
        }
        if (pm.ammunition < 10) {
            pm.ammunition = 0;
        } else {
            pm.ammunition -= 10;
        }
        return GetDamage();
    }
};

class Army
{
    std::vector <Unit *> units;
public:
    Army() : units() {}
    ~Army()
    {
        for (uint i = 0; i < units.size(); i++) {
            delete units[i];
        }
    }
    void AddUnit(Unit *unit)
    {
        units.push_back(unit);
    }
    Unit * operator[](const int num)
    {
        return units[num];
    }
};

class City
{
public:
    City() : army() {}
    virtual ~City() {}
    virtual void CreateUnit(unit_type un_type) = 0;
    Army army;
};

class Castle : public City
{
public:
    Castle() {}
    virtual ~Castle() {}
    virtual void CreateUnit(unit_type un_type)
    {
        switch(un_type) {
        case SHOOTING:
            army.AddUnit(new Archery());
            break;
        case PRANCING:
            army.AddUnit(new Knight());
            break;
        case MAGICIAN:
            army.AddUnit(new Monk());
            break;
        default:
            break;
        }
    }
};

class Tower : public City
{
public:
    Tower() {}
    virtual ~Tower() {}
    virtual void CreateUnit(unit_type un_type)
    {
        switch(un_type) {
        case SHOOTING:
            army.AddUnit(new MasterGremlin());
            break;
        case PRANCING:
            army.AddUnit(new Golem());
            break;
        case MAGICIAN:
            army.AddUnit(new Wizard());
            break;
        default:
            break;
        }
    }
};

class Game {
    City *city;
    Game(const Game &) = delete;
    Game & operator =(const Game &) = delete;
public:
    Game() : city(nullptr)
    {
        srand(time(NULL));
        int tp;
        std::cin >> tp;
        if (tp == CASTLE) {
            city = new Castle();
        } 
        if (tp == TOWER) {
            city = new Tower();
        }
    }
    ~Game()
    {
        if (city != nullptr) {
            delete city;
        }
    }
    void DoWork()
    {
        int tp;
        while (std::cin >> tp) {
            city->CreateUnit(unit_type(tp));
        }
    }
};

int main()
{
    Game hmm1;
    hmm1.DoWork();
    return 0;
}