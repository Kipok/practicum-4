#include <iostream>
#include <cstdio>
#include <algorithm>
#include <iterator>
#include <utility>
#include <string>
#include <vector>
#include <list>
using namespace std;

typedef unsigned int uint;

void process(const vector <int> &v1, vector <int> &v2)
{
    int i = 0;
    int last = v2.size();
    for (auto v1_it = v1.begin(); v1_it != v1.end() && i < last; ++v1_it, ++i) {
        auto v2_it = v2.begin() + i;
        if (*v1_it > *v2_it) {
            v2.push_back(*v1_it);
        }
    }
    copy(v2.begin(), v2.end(), ostream_iterator<int>(cout, " "));
}