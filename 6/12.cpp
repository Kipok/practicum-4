#include <iostream>
#include <cstdio>
#include <stdexcept>
#include <string>
#include <fstream>
#include <vector>
#include <iterator>
#include <map>
#include <ctime>
#include <sstream>
#include <iomanip>
#include <algorithm>
using namespace std;

class Date
{
    string s;
public:
    Date(string s_) : s()
    {
        char c;
        stringstream ss(s_);
        int year, mon, day;
        ss >> year >> c >> mon >> c >> day;
        ss.clear();
        ss << year << '/' << setfill('0') << setw(2) << mon << '/' << setfill('0') << setw(2) << day;
        s = ss.str();
    }
    string str() const
    {
        return s;
    }
    bool operator <(const Date &d) const
    {
        return s < d.s;
    }
    bool operator ==(const Date &d) const
    {
        return s == d.s;
    }
};

void print_table(map <string, map <Date, int> > &m, vector <Date> &dates, int max_len)
{
    cout.setf(ios::left);
    cout.width(max_len + 2);
    cout << '.';
    for (unsigned int i = 0; i < dates.size(); i++) {
        cout.width(12);
        cout << dates[i].str();
    }
    cout << endl;
    for (auto it = m.begin(); it != m.end(); ++it) {
        cout.width(max_len + 2);
        cout << it->first;
        for (unsigned int i = 0; i < dates.size(); i++) {
            cout.width(12);
            auto cur_it = it->second.find(dates[i]);
            if (cur_it != it->second.end()) {
                cout << cur_it->second;
            }
            else {
                cout << '.';
            }
        }
        cout << endl;
    }
}

int main()
{
    freopen("02-3.in", "rt", stdin);
    freopen("02-3.out", "wt", stdout);
    map <string, map <Date, int> > m;
    vector <Date> dates;
    string s, ds;
    int g;
    int max_len = 0;
    while (cin >> s >> ds >> g) {
        if (m.count(s) == 0) {
            map <Date, int> tmp;
            tmp.insert(make_pair(Date(ds), g));
            m.insert(make_pair(s, tmp));
        }
        else {
            m[s][ds] = g;
        }
        max_len = max(unsigned(max_len), s.size());
        dates.push_back(Date(ds));
    }
    sort(dates.begin(), dates.end());
    dates.erase(unique(dates.begin(), dates.end()), dates.end());
    print_table(m, dates, max_len);
    return 0;
}