#include <iostream>
#include <cstdio>
#include <stdexcept>
#include <string>
#include <fstream>
#include <vector>
#include <iterator>
#include <map>
using namespace std;

int main()
{
    freopen("02-2.in", "rt", stdin);
    freopen("02-2.out", "wt", stdout);
    map <string, pair <double, int> > mp;
    string s;
    double d;
    while (cin >> s >> d) {
        if (mp.count(s) == 0) {
            mp.insert(make_pair(s, make_pair(d, 1)));
        }
        else {
            mp[s].first += d;
            ++mp[s].second;
        }
    }
    for (auto it = mp.begin(); it != mp.end(); ++it) {
        cout << it->first << ' ' << it->second.first / it->second.second << endl;
    }
    return 0;
}