#include <iostream>
#include <cstdio>
#include <algorithm>
#include <iterator>
#include <utility>
#include <string>
#include <vector>
using namespace std;

void process(vector <int> &v)
{
    for (auto it = v.begin(); it != v.end(); ++it) {
        it = v.erase(it);
        if (it == v.end()) {
            break;
        }
    }
    copy(v.rbegin(), v.rend(), ostream_iterator<int>(cout, " "));
}