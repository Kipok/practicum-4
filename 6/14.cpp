#include <iostream>
#include <cstdio>
#include <stdexcept>
#include <string>
#include <fstream>
#include <vector>
#include <iterator>
#include <map>
#include <ctime>
#include <sstream>
#include <iomanip>
#include <algorithm>
using namespace std;

template <class it_type>
void myreverse(it_type it1, it_type it2)
{
    while (it1 != it2 && it1 != --it2) {
        swap(*it1, *it2);
        ++it1;
    }
}
