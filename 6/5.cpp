#include <iostream>
#include <cstdio>
#include <algorithm>
#include <iterator>
#include <utility>
#include <string>
#include <vector>
#include <list>
using namespace std;

typedef unsigned int uint;

bool comp(const uint &a, const uint &b)
{
    int c_a = 0, c_b = 0;
    for (uint i = 0; i < 32; i++) {
        c_a += !!(a & (1u << i));
        c_b += !!(b & (1u << i));
    }
    return c_a < c_b;
}

int main()
{
    vector <uint> v;
    copy(istream_iterator<uint>(cin), istream_iterator<uint>(), back_inserter(v));
    stable_sort(v.begin(), v.end(), comp);
    copy(v.begin(), v.end(), ostream_iterator<uint>(cout, "\n"));
    return 0;
}