#include <iostream>
#include <cstdio>
#include <stdexcept>
#include <string>
#include <fstream>
#include <vector>
#include <iterator>
using namespace std;

int main(int argc, char *argv[])
{
    freopen("02-1.out", "wt", stdout);
    vector <pair <string, vector <int> > > t;
    vector <double> gr;
    string s;
    for (int i = 1; i < argc; i++) {
        ifstream in(argv[i]);
        while (in >> s) {
            vector <int> tmp;
            int cur_g = 0;
            double cur_mg = 0;
            int cur_num = 0;
            while (in >> cur_g) {
                if (cur_g == 0) {
                    break;
                }
                tmp.push_back(cur_g);
                ++cur_num;
                cur_mg += cur_g;
            }
            t.push_back(make_pair(string(argv[i]) + " " + s, tmp));
            gr.push_back(cur_mg / cur_num);
        }
    }
    double mg = 0;
    for (auto it = gr.begin(); it != gr.end(); ++it) {
        mg += *it;
    }
    mg /= gr.size();
    for (unsigned int j = 0; j < gr.size(); j++) {
        if (gr[j] + 1e-7 >= mg) {
            cout << t[j].first << ' ';
            copy(t[j].second.begin(), t[j].second.end(), ostream_iterator<int>(cout, " "));
            cout << endl;
        }
    }
    return 0;
}