#include <iostream>
#include <cstdio>
#include <algorithm>
#include <iterator>
#include <utility>
#include <string>
using namespace std;

void print_reverse(const string &s)
{
    string r_s = s;
    for (unsigned int i = 0; i < s.size(); i++) {
        reverse(r_s.begin(), r_s.end() - i);
        if (r_s.compare(0, r_s.size() - i, s, 0, s.size() - i) == 0) {
            copy(r_s.begin(), r_s.end() - i, ostream_iterator<char>(std::cout));
            cout << endl;
            break;
        }
        reverse(r_s.begin(), r_s.end() - i);
    }
}

int main() {
    string s;
    while (cin >> s) {
        print_reverse(s);
    }
    return 0;
}