#include <iostream>
#include <cstdio>
#include <stdexcept>
#include <string>
#include <fstream>
#include <vector>
#include <iterator>
#include <map>
#include <ctime>
#include <sstream>
#include <iomanip>
#include <algorithm>
#include <set>
#include <deque>
using namespace std;

int main()
{
    deque <pair <int, int> > dq;
    int n, k, a;
    cin >> n >> k >> a;
    dq.push_back(make_pair(a, 0));
    for (int i = 1; i < k; i++) {
        cin >> a;
        while (!dq.empty() && dq.back().first > a) {
            dq.pop_back();
        }
        dq.push_back(make_pair(a, i));
    }
    for (int i = k; i < n; i++) {
        cout << dq.front().first << endl;
        if (dq.front().second == i - k) {
            dq.pop_front();
        }
        cin >> a;
        while (!dq.empty() && dq.back().first > a) {
            dq.pop_back();
        }
        dq.push_back(make_pair(a, i));
    }
    cout << dq.front().first << endl;
    return 0;
}