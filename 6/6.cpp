#include <iostream>
#include <cstdio>
#include <algorithm>
#include <iterator>
#include <utility>
#include <string>
#include <vector>
#include <list>
using namespace std;

typedef unsigned int uint;

void process(const vector <int> &v, list <int> &l, uint step)
{
    auto l_it = l.begin();
    for (auto v_it = v.begin(); v_it != v.end() && l_it != l.end(); v_it += step, ++l_it) {
        *l_it = *v_it;
        if (step >= v.size() || v_it >= v.end() - step) {
            break;
        }
    }
    copy(l.rbegin(), l.rend(), ostream_iterator<int>(cout, " "));
}