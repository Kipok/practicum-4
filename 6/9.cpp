#include <iostream>
#include <cstdio>
#include <algorithm>
#include <iterator>
#include <utility>
#include <string>
#include <vector>
#include <list>
using namespace std;

typedef unsigned int uint;

void process(const vector <int> &v, list <int> &l)
{
    vector <int> tmp = v;
    sort(tmp.begin(), tmp.end());
    tmp.erase(unique(tmp.begin(), tmp.end()), tmp.end());
    auto v_it = tmp.rbegin();
    int num = l.size();
    auto l_it = l.rbegin();
    while (v_it != tmp.rend() && *v_it > num) {
        ++v_it;
    }
    while (v_it != tmp.rend() && *v_it > 0 && l_it != l.rend()) {
        while (l_it != l.rend() && num > *v_it) {
            --num;
            ++l_it;
        }
        if (l_it != l.rend()) {
            ++l_it;
            l_it = list<int>::reverse_iterator(l.erase(l_it.base()));
            --num;
            ++v_it;
        }
    }
}