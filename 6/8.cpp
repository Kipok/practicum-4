#include <iostream>
#include <cstdio>
#include <algorithm>
#include <iterator>
#include <utility>
#include <string>
#include <vector>
#include <list>
using namespace std;

typedef unsigned int uint;

void process(vector <int> &v1, const vector <int> &v2, int k)
{
    for (auto v2_it = v2.begin(); v2_it != v2.end(); ++v2_it) {
        if (*v2_it <= int(v1.size()) && *v2_it >= 0) {
            auto v1_it = v1.begin() + *v2_it - 1;
            *v1_it *= k;
        }
    }
    copy(v1.begin(), v1.end(), ostream_iterator<int>(cout, " "));
}