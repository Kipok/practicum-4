#include <iostream>
#include <cstdio>
#include <stdexcept>
#include <string>
#include <fstream>
#include <vector>
#include <iterator>
#include <map>
#include <ctime>
#include <sstream>
#include <iomanip>
#include <algorithm>
using namespace std;

template <class it_type, class pred_type>
int myfilter(it_type it1, it_type it2, pred_type pred, 
            typename it_type::value_type value = typename it_type::value_type())
{
    int ans = 0;
    while (it1 != it2) {
        if (pred(*it1)) {
            *it1 = value;
            ++ans;
        }
        ++it1;
    }
    return ans;
}