#include <iostream>
#include <cstdio>
#include <stdexcept>
#include <string>
#include <fstream>
#include <vector>
#include <iterator>
#include <map>
#include <ctime>
#include <sstream>
#include <iomanip>
#include <algorithm>
using namespace std;

template <class cont_type>
typename cont_type::value_type process(cont_type &c)
{
    auto it = c.rbegin();
    for (; it != c.rend() && distance(c.rbegin(), it) < 3; ++it) {}
    typename cont_type::value_type sum = typename cont_type::value_type();
    for (auto i = c.rbegin(); i != it; ++i) {
        sum += *i;
    }
    return sum;
}