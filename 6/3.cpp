#include <iostream>
#include <cstdio>
#include <algorithm>
#include <iterator>
#include <utility>
#include <string>
#include <vector>
#include <list>
using namespace std;

typedef unsigned int uint;

int process(vector <int> &v, const list <int> &l, uint step)
{
    auto l_it = l.begin();
    int ans = 0;
    for (auto v_it = v.begin(); v_it != v.end() && l_it != l.end(); v_it += step, ++l_it) {
        if (*l_it < 0) {
            *v_it = *l_it;
            ++ans;
        }
        if (step >= v.size() || v_it >= v.end() - step) {
            break;
        }
    }
    copy(v.begin(), v.end(), ostream_iterator<int>(cout, " "));
    return ans;
}