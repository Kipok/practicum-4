#include <iostream>
#include <sstream>
#include <fstream>
#include <iomanip>
#include <cstdio>
#include <vector>
#include <string>
#include <ctime>

using namespace std;

enum {
    RUSSIAN_MODE = 1,
    USA_MODE = 2,
    FULL_MODE = 3
};

class DateFormatter
{
public:
    virtual string printDate(tm t) const = 0;
    virtual ~DateFormatter() {}
    static DateFormatter * create(int mode);
};

class RussianDateFormatter : public DateFormatter
{
public:
    RussianDateFormatter() {}
    virtual ~RussianDateFormatter() {}
    virtual string printDate(tm t) const {
        char str[30];
        strftime(str, 100, "%d.%m.%Y", &t);
        return str;
    }
};

class USADateFormatter : public DateFormatter
{
public:
    USADateFormatter() {}
    virtual ~USADateFormatter() {}
    virtual string printDate(tm t) const {
        char str[30];
        strftime(str, 100, "%Y-%m-%d", &t);
        return str;
    }
};

class FullDateFormatter : public DateFormatter
{
public:
    FullDateFormatter() {}
    virtual ~FullDateFormatter() {}
    virtual string printDate(tm t) const {
        char str[30];
        strftime(str, 100, "%d %b %Y", &t);
        return str;
    }
};

DateFormatter * DateFormatter::create(int mode) 
{
    DateFormatter *df = nullptr;
    switch (mode) {
    case RUSSIAN_MODE:
        df = new RussianDateFormatter();
        break;
    case USA_MODE:
        df = new USADateFormatter();
        break;
    case FULL_MODE:
        df = new FullDateFormatter();
        break;
    default:
        break;
    }
    return df;
}

class TimeFormatter
{
public:
    virtual string printTime(tm t) const = 0;
    virtual ~TimeFormatter() {}
    static TimeFormatter * create(int mode);
};

class RussianTimeFormatter : public TimeFormatter
{
public:
    RussianTimeFormatter() {}
    virtual ~RussianTimeFormatter() {}
    virtual string printTime(tm t) const {
        char str[30];
        strftime(str, 100, "%H:%M", &t);
        return str;
    }
};

class USATimeFormatter : public TimeFormatter
{
public:
    USATimeFormatter() {}
    virtual ~USATimeFormatter() {}
    virtual string printTime(tm t) const {
        char str[30];
        strftime(str, 100, "%I:%M %p", &t);
        return str;
    }
};

TimeFormatter * TimeFormatter::create(int mode) 
{
    TimeFormatter *df = nullptr;
    switch (mode) {
    case RUSSIAN_MODE:
        df = new RussianTimeFormatter();
        break;
    case USA_MODE:
        df = new USATimeFormatter();
        break;
    default:
        break;
    }
    return df;
}

void run(string filename) 
{
    ifstream fin(filename);
    int mode;
    fin >> mode;
    fin.close();
    DateFormatter *df = DateFormatter::create(mode);
    TimeFormatter *tf = TimeFormatter::create(mode);
    f(df, tf);
    delete df;
    delete tf;
}
