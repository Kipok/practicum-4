#include <iostream>
#include <sstream>
#include <fstream>
#include <iomanip>
#include <cstdio>
#include <vector>
#include <string>
#include <ctime>

using namespace std;

enum {
    RUSSIAN_MODE = 1,
    USA_MODE = 2,
    FULL_MODE = 3
};

class DateFormatter
{
public:
    virtual string printDate(tm t) const = 0;
    virtual ~DateFormatter() {}
    static DateFormatter * create(int mode);
};

class RussianDateFormatter : public DateFormatter
{
public:
    RussianDateFormatter() {}
    virtual ~RussianDateFormatter() {}
    virtual string printDate(tm t) const {
        char str[30];
        strftime(str, 100, "%d.%m.%Y", &t);
        return str;
    }
};

class USADateFormatter : public DateFormatter
{
public:
    USADateFormatter() {}
    virtual ~USADateFormatter() {}
    virtual string printDate(tm t) const {
        char str[30];
        strftime(str, 100, "%Y-%m-%d", &t);
        return str;
    }
};

class FullDateFormatter : public DateFormatter
{
public:
    FullDateFormatter() {}
    virtual ~FullDateFormatter() {}
    virtual string printDate(tm t) const {
        char str[30];
        strftime(str, 100, "%d %b %Y", &t);
        return str;
    }
};

DateFormatter * DateFormatter::create(int mode) 
{
    DateFormatter *df = nullptr;
    switch (mode) {
    case RUSSIAN_MODE:
        df = new RussianDateFormatter();
        break;
    case USA_MODE:
        df = new USADateFormatter();
        break;
    case FULL_MODE:
        df = new FullDateFormatter();
        break;
    default:
        break;
    }
    return df;
}

class MutableDateFormatter : public DateFormatter
{
    DateFormatter *df;
    MutableDateFormatter(const MutableDateFormatter &) = delete;
    MutableDateFormatter &operator=(const MutableDateFormatter &) = delete;
public:
    MutableDateFormatter(int mode) : df(nullptr) {
        df = DateFormatter::create(mode);
    }
    void toType(int mode) {
        delete df;
        df = DateFormatter::create(mode);
    }
    virtual string printDate(tm t) const {
        return df->printDate(t);
    }
    virtual ~MutableDateFormatter() {
        delete df;
    }
};

void run(string filename) 
{
    ifstream fin(filename);
    int mode;
    fin >> mode;
    fin.close();
    MutableDateFormatter *df = new MutableDateFormatter(mode);
    f(df);
    delete df;
}