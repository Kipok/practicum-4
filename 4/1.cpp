#include <iostream>
#include <sstream>
#include <fstream>
#include <iomanip>
#include <cstdio>
#include <vector>
#include <string>
#include <ctime>

class DateFormater
{
    int mode;
public:
    DateFormater(int mode_) : mode(mode_) {}
    std::string printDate(tm t) {
        std::stringstream ss;
        if (mode == 1) {
            ss  << std::setfill('0') << std::setw(2) << t.tm_mday << '.'
                << std::setw(2) << t.tm_mon + 1 << '.'
                << t.tm_year + 1900;
        } else {
            ss  << t.tm_year + 1900 << '-'
                << std::setfill('0') << std::setw(2) << t.tm_mon + 1 << '-'
                << std::setw(2) << t.tm_mday;
        }
        return ss.str();
    }
    ~DateFormater() {}
};

void run(std::string filename) 
{
    std::ifstream fin(filename);
    int mode;
    fin >> mode;
    fin.close();
    DateFormater *df = new DateFormater(mode);
    f(df);
    delete df;
}