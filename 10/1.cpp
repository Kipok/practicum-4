#include <iostream>
#include <cstdio>
#include <vector>
#include <string>
#include <memory.h>
#include <cstdlib>
#include <algorithm>
#include <map>
#include <set>
#include <iterator>
using namespace std;

class Grammar
{
    template <class T>
    struct GrammarSet
    {
        T data[26];
        T & operator [](char c)
        {
            return data[c - 'A'];
        }
    };
    map <char, vector <string>> rules;
    GrammarSet <bool> isNullable;
    GrammarSet <set <char>> first, follow;
public:
    inline bool isNT(char c)
    {
        return c >= 'A' && c <= 'Z';
    }
    void readRules()
    {
        memset(isNullable.data, false, sizeof(isNullable.data));
        char c;
        string s;
        while (getline(cin, s)) {
            c = s[0];
            if (s.size() == 1) {
                s = "";
            }
            if (s.size() >= 2) {
                s.erase(s.begin(), s.begin() + 2);
            }
            if (s.empty()) {
                s = " ";
                isNullable[c] = true;
            }
            rules[c].push_back(s);
        }
        for (auto it : rules) {
            sort(it.second.begin(), it.second.end());
            it.second.erase(unique(it.second.begin(), it.second.end()), it.second.end());
        }
    }
    void fillNullable()
    {
        bool f = true;
        while (f) {
            f = false;
            for (auto cur_rules : rules) {
                if (isNullable[cur_rules.first] == false) {
                    for (auto rule : cur_rules.second) {
                        bool sign = true;
                        for (auto ch : rule) {
                            if (isNT(ch) == false || isNullable[ch] == false) {
                                sign = false;
                                break;
                            }
                        }
                        if (sign == true) {
                            f = true;
                            isNullable[cur_rules.first] = true;
                            break;
                        }
                    }
                }
            }
        }
    }
    void printNullable()
    {
        for (auto cur_rules : rules) {
            cout << cur_rules.first << ' ' << (isNullable[cur_rules.first] ? 1 : 0) << endl;
        }
    }
    void fillFirst()
    {
        for (auto cur_rules : rules) {
            for (auto rule : cur_rules.second) {
                for (auto ch : rule) {
                    if (ch != cur_rules.first) {
                        first[cur_rules.first].insert(ch);
                    }
                    if (isNT(ch) == false || isNullable[ch] == false) {
                        break;
                    }
                }
            }
        }
        bool f = true;
        while (f) {
            f = false;
            for (auto cur_rules : rules) {
                for (auto cur_ch : first[cur_rules.first]) {
                    if (isNT(cur_ch) == false) {
                        continue;
                    }
                    unsigned int size = first[cur_rules.first].size();
                    first[cur_rules.first].insert(first[cur_ch].begin(), first[cur_ch].end());
                    if (size != first[cur_rules.first].size()) {
                        f = true;
                    }
                }
            }
        }
        for (auto cur_rules : rules) {
            for (auto cur_ch = first[cur_rules.first].begin(); cur_ch != first[cur_rules.first].end();) {
                if (isNT(*cur_ch) || *cur_ch == ' ') {
                    first[cur_rules.first].erase(cur_ch++);
                }
                else {
                    ++cur_ch;
                }
            }
        }
    }
    void printFirst()
    {
        for (auto cur_rules : rules) {
            cout << cur_rules.first << ' ';
            copy(first[cur_rules.first].begin(), first[cur_rules.first].end(), ostream_iterator<char>(cout));
            cout << endl;
        }
    }
    void fillFollow()
    {
        for (auto cur_rules : rules) {
            for (auto rule : cur_rules.second) {
                for (unsigned int i = 0; i < rule.size(); i++) {
                    if (isNT(rule[i])) {
                        unsigned int j = i;
                        while (j < rule.size()) {
                            ++j;
                            if (j == rule.size()) {
                                if (rule[i] != cur_rules.first) {
                                    follow[rule[i]].insert(cur_rules.first);
                                }
                                break;
                            }
                            if (isNT(rule[j])) {
                                follow[rule[i]].insert(first[rule[j]].begin(), first[rule[j]].end());
                            }
                            else {
                                follow[rule[i]].insert(rule[j]);
                            }
                            if (isNT(rule[j]) == false || isNullable[rule[j]] == false) {
                                break;
                            }
                        }
                    }
                }
            }
        }

        bool f = true;
        while (f) {
            f = false;
            for (auto cur_rules : rules) {
                for (auto cur_ch : follow[cur_rules.first]) {
                    if (isNT(cur_ch) == false) {
                        continue;
                    }
                    unsigned int size = follow[cur_rules.first].size();
                    follow[cur_rules.first].insert(follow[cur_ch].begin(), follow[cur_ch].end());
                    if (size != follow[cur_rules.first].size()) {
                        f = true;
                    }
                }
            }
        }

        for (auto cur_rules : rules) {
            for (auto cur_ch = follow[cur_rules.first].begin(); cur_ch != follow[cur_rules.first].end();) {
                if (isNT(*cur_ch) || *cur_ch == ' ') {
                    follow[cur_rules.first].erase(cur_ch++);
                }
                else {
                    ++cur_ch;
                }
            }
        }

    }
    void printFollow()
    {
        for (auto cur_rules : rules) {
            cout << cur_rules.first << ' ';
            copy(follow[cur_rules.first].begin(), follow[cur_rules.first].end(), ostream_iterator<char>(cout));
            cout << endl;
        }
    }
    Grammar() : rules(), isNullable(), first(), follow() {}
};

int main()
{
    Grammar g;
    g.readRules();
    g.fillNullable();
    g.printNullable();
    g.fillFirst();
    g.printFirst();
    g.fillFollow();
    g.printFollow();
    return 0;
}