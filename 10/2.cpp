#include <iostream>
#include <cstdio>
#include <vector>
#include <string>
#include <memory.h>
#include <cstdlib>
#include <algorithm>
#include <map>
#include <set>
#include <iterator>
#include <sstream>
using namespace std;

struct MyEx
{
    char c;
    MyEx(char c_) : c(c_) {}
};

void B(stringstream &sin)
{
    char c;
    sin >> c;
    if (c != '0' && c != '1') {
        throw MyEx(c);
    }
    if (c == '0') {
        cout << '1';
        B(sin);
        sin >> c;
        if (c != '1') {
            throw MyEx(c);
        }
        else {
            cout << '0';
        }
        return;
    }
    if (c == '1') {
        cout << '0';
    }
}

void A(stringstream &sin)
{
    char c;
    sin >> c;
    if (c != '1' && c != '0') {
        throw MyEx(c);
    }
    if (c == '1') {
        A(sin);
        sin >> c;
        if (c != '0') {
            throw MyEx(c);
        }
        else {
            cout << '0';
        }
        return;
    }
    if (c == '0') {
        cout << '1';
        B(sin);
        sin >> c;
        if (c != '0') {
            throw MyEx(c);
        }
        else {
            cout << '0';
        }
    }
}

void S(stringstream &sin)
{
    char c;
    sin >> c;
    if (c != '1') {
        throw MyEx(c);
    }
    A(sin);
}

//S -> 1'A
//A -> 1'A0" | 0'B0"
//B -> 0'B1" | 1"

int main()
{
    string s;
    while (cin >> s) {
        try {
            stringstream sin(s);
            S(sin);
            cout << endl;
        }
        catch (MyEx e) {
            cout << "An error near " << e.c << " symbol\n";
            return 0;
        }
    }
    return 0;
}