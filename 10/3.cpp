#include <iostream>
#include <cstdio>
#include <vector>
#include <string>
#include <cstdlib>
#include <algorithm>
#include <map>
#include <set>
#include <iterator>
#include <memory>
using namespace std;

// d -- digit; 
// o -- operand
// N -- number state
// O -- operand state

// S -> oO | dN | .L
// N -> oO | dN | .L
// O -> oO | dN | .L
// L -> eps

class Automat
{
    class State
    {
        string info;
    public:
        map <char, shared_ptr<State>> tr;
        State(map <char, shared_ptr<State>> &m) : info(), tr(m) {}
        State() : info(), tr() {}
        void init(map <char, shared_ptr<State>> &m)
        {
            tr = m;
        }
        shared_ptr<State> operator[](char c) 
        { 
            return tr[c];
        }
        void addInfo(char c)
        {
            info += c;
        }
        void resetInfo()
        {
            info.clear();
        }
        void printInfo() const
        {
            cout << info << endl;
        }
    };
    shared_ptr <State> cur_state, N, O, L, S;
public:
    Automat() : cur_state(nullptr), N(nullptr), O(nullptr), L(nullptr), S(nullptr)
    {
        N = make_shared<State>();
        O = make_shared<State>();
        L = make_shared<State>();
        S = make_shared<State>();
        map <char, shared_ptr<State>> tmp;
        const string digits = "0123456789";
        const string operands = "+-()*%/";
        // initializing S
        for (auto d : digits) {
            tmp.insert(make_pair(d, N));
        }
        for (auto op : operands) {
            tmp.insert(make_pair(op, O));
        }
        tmp.insert(make_pair('.', L));
        S->init(tmp);
        // initializing N
        N->init(tmp);
        // initializing O
        O->init(tmp);
        cur_state = S;
    }
    bool processDigit()
    {
        char c;
        cin >> c;
        if (cur_state == O && cur_state->tr[c] == N) {
            cur_state->printInfo();
            cur_state->resetInfo();
        }
        if (cur_state->tr[c] == O || cur_state->tr[c] == L) {
            cur_state->printInfo();
            cur_state->resetInfo();
        }
        cur_state->tr[c]->addInfo(c);
        cur_state = cur_state->tr[c];
        if (cur_state == L) {
            return false;
        }
        return true;
    }
};

int main()
{
    Automat a;
    while (a.processDigit()) {}
    return 0;
}