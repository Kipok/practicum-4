#include <iostream>
#include <cstdio>
#include <vector>
#include <string>
#include <ctime>
#include <memory>
#include <fstream>
#include <set>
#include <map>
#include <algorithm>

//it is just some enums and types. It should be clear
typedef unsigned int uint;
typedef uint h_type;
typedef uint d_type;
typedef uint amm_type;
typedef uint exp_type;
typedef uint arm_type;
typedef uint speed_type;
#define inf -1

enum town_type {
    CASTLE = 1,
    TOWER = 2,
    RAMPART = 3
};

enum unit_type {
    SHOOTING = 1,
    PRANCING = 2,
    MAGICIAN = 3,
    GREAT_UNIT = 4
};

enum attack_type {
    DISTANCE,
    CLOSE
};

enum mana_values {
    MAX_MANA = 50
};

enum battle_modes {
    HANDLE = 1,
    AUTOMATIC = 2
};

enum command_type {
    HERO_SPELL,
    UNIT_ATTACK
};

// visitor class for commands
class CommandVisitor
{
public:
    CommandVisitor() {}
    virtual ~CommandVisitor() {}
    virtual void visit(std::shared_ptr<class Archery>) = 0;
    virtual void visit(std::shared_ptr<class Knight>) = 0;
    virtual void visit(std::shared_ptr<class Monk>) = 0;
    virtual void visit(std::shared_ptr<class Angel>) = 0;

    virtual void visit(std::shared_ptr<class MasterGremlin>) = 0;
    virtual void visit(std::shared_ptr<class Golem>) = 0;
    virtual void visit(std::shared_ptr<class Wizard>) = 0;
    virtual void visit(std::shared_ptr<class Titan>) = 0;

    virtual void visit(std::shared_ptr<class WoodElf>) = 0;
    virtual void visit(std::shared_ptr<class Centaur>) = 0;
    virtual void visit(std::shared_ptr<class Unicorn>) = 0;
    virtual void visit(std::shared_ptr<class GreenDragon>) = 0;
};

// this is the basic Unit class. All units are derived from this one
class Unit
{
protected:
    // it is just a convenient way to keep unit parameters
    struct UnitParams {
        std::string name;
        h_type health;
        attack_type attack;
        amm_type ammunition;
        exp_type exp;
        arm_type armor;
        speed_type speed;
        UnitParams(std::string n, h_type h, attack_type a, amm_type amm) :
            name(n), health(h), attack(a), ammunition(amm), exp(0), armor(0), speed(0) {}
    };
    UnitParams pm;

    // I decided to hold id here because it is a useful way to know if a unit is inside army.
    // There is no difference where to keep it while I carefully set it to valid values when moving
    // between armies.
    int id;

public:
    Unit(UnitParams pm_) : pm(pm_), id(0) {}
    virtual ~Unit() {}

    virtual inline d_type GetDamage() const
    {
        return 10;
    }

    h_type GetHealth() const
    {
        return pm.health;
    }

    void SetHealth(h_type hl)
    {
        pm.health = hl;
    }

    amm_type GetAmmo() const
    {
        return pm.ammunition;
    }

    void SetAmmo(amm_type ammo)
    {
        pm.ammunition = ammo;
    }

    speed_type GetSpeed() const
    {
        return pm.speed;
    }

    void SetSpeed(speed_type sp)
    {
        pm.speed = sp;
    }

    UnitParams GetPM() const
    {
        return pm;
    }

    void IncExp()
    {
        ++pm.exp;
    }

    // This function returns Unit id
    int GetId() const
    {
        return id;
    }

    // This function sets Unit id -- when Unit is removed from the army, id = -1.
    void SetId(int id_)
    {
        id = id_;
    }

    virtual void accept(CommandVisitor &) = 0;

    std::string GetName() const
    {
        return pm.name;
    }
};

// it is a class for shooting units (later all shooting units may have the same features, different from others)
class Shooting : public Unit
{
public:
    Shooting(UnitParams pm_) : Unit(pm_) {}
    virtual ~Shooting() {}
};

// it is a class for prancing units
class Prancing : public Unit
{
public:
    Prancing(UnitParams pm_) : Unit(pm_) {}
    virtual ~Prancing() {}
};

// it is a class for magician units
class Magician : public Unit
{
public:
    Magician(UnitParams pm_) : Unit(pm_) {}
    virtual ~Magician() {}
};

// it is a class for great units
class GreatUnit : public Unit
{
public:
    GreatUnit(UnitParams pm_) : Unit(pm_) {}
    virtual ~GreatUnit() {}
};

// Archery -- Castle shooting unit
class Archery : public Shooting, public std::enable_shared_from_this < Archery >
{
public:
    Archery(UnitParams pm_) : Shooting(pm_) {}
    Archery() : Shooting(UnitParams("Archery", 45, DISTANCE, 15)) {}
    virtual ~Archery() {}
    virtual inline d_type GetDamage() const override
    {
        return 10;
    }
    void accept(CommandVisitor &v)
    {
        v.visit(shared_from_this());
    }
};

// Knight -- Castle prancing unit
class Knight : public Prancing, public std::enable_shared_from_this < Knight >
{
public:
    Knight(UnitParams pm_) : Prancing(pm_) {}
    Knight() : Prancing(UnitParams("Knight", 145, CLOSE, inf)) {}
    virtual ~Knight() {}
    virtual inline d_type GetDamage() const override
    {
        return 20;
    }
    void accept(CommandVisitor &v)
    {
        v.visit(shared_from_this());
    }
};

// Monk -- Castle magician unit
class Monk : public Magician, public std::enable_shared_from_this < Monk >
{
public:
    Monk(UnitParams pm_) : Magician(pm_) {}
    Monk() : Magician(UnitParams("Monk", 100, DISTANCE, 30)) {}
    virtual ~Monk() {}
    virtual inline d_type GetDamage() const override
    {
        return 20 + (rand() % 11);
    }
    void accept(CommandVisitor &v)
    {
        v.visit(shared_from_this());
    }
};

// Angel -- Castle great unit
class Angel : public GreatUnit, public std::enable_shared_from_this < Angel >
{
public:
    Angel(UnitParams pm_) : GreatUnit(pm_) {}
    Angel() : GreatUnit(UnitParams("Angel", 345, CLOSE, inf)) {}
    virtual ~Angel() {}
    void accept(CommandVisitor &v)
    {
        v.visit(shared_from_this());
    }
    virtual inline d_type GetDamage() const override
    {
        return 80;
    }
};

// Tower shooting unit
class MasterGremlin : public Shooting, public std::enable_shared_from_this < MasterGremlin >
{
public:
    MasterGremlin(UnitParams pm_) : Shooting(pm_) {}
    MasterGremlin() : Shooting(UnitParams("MasterGremlin", 25, DISTANCE, 10)) {}
    virtual ~MasterGremlin() {}
    void accept(CommandVisitor &v)
    {
        v.visit(shared_from_this());
    }
    virtual inline d_type GetDamage() const override
    {
        return 15;
    }
};

// Tower prancing unit
class Golem : public Prancing, public std::enable_shared_from_this < Golem >
{
public:
    Golem(UnitParams pm_) : Prancing(pm_) {}
    Golem() : Prancing(UnitParams("Golem", 175, CLOSE, inf)) {}
    virtual ~Golem() {}
    virtual inline d_type GetDamage() const override
    {
        return 30;
    }
    void accept(CommandVisitor &v)
    {
        v.visit(shared_from_this());
    }
};

// Tower magician unit
class Wizard : public Magician, public std::enable_shared_from_this < Wizard >
{
public:
    Wizard(UnitParams pm_) : Magician(pm_) {}
    Wizard() : Magician(UnitParams("Wizard", 90, DISTANCE, 50)) {}
    virtual ~Wizard() {}
    virtual inline d_type GetDamage() const override
    {
        return 30 + (rand() % 21);
    }
    void accept(CommandVisitor &v)
    {
        v.visit(shared_from_this());
    }
};

// Titan -- Tower great unit
class Titan : public GreatUnit, public std::enable_shared_from_this < Titan >
{
public:
    Titan(UnitParams pm_) : GreatUnit(pm_) {}
    Titan() : GreatUnit(UnitParams("Titan", 400, DISTANCE, inf)) {}
    virtual ~Titan() {}
    virtual inline d_type GetDamage() const override
    {
        return 60;
    }
    void accept(CommandVisitor &v)
    {
        v.visit(shared_from_this());
    }
};

// Rampart shooting unit
class WoodElf : public Shooting, public std::enable_shared_from_this < WoodElf >
{
public:
    WoodElf(UnitParams pm_) : Shooting(pm_) {}
    WoodElf() : Shooting(UnitParams("Wood Elf", 30, DISTANCE, 12)) {}
    virtual ~WoodElf() {}
    virtual inline d_type GetDamage() const override
    {
        return 20;
    }
    void accept(CommandVisitor &v)
    {
        v.visit(shared_from_this());
    }
};

// Rampart prancing unit
class Centaur : public Prancing, public std::enable_shared_from_this < Centaur >
{
public:
    Centaur(UnitParams pm_) : Prancing(pm_) {}
    Centaur() : Prancing(UnitParams("Centaur", 150, CLOSE, inf)) {}
    virtual ~Centaur() {}
    virtual inline d_type GetDamage() const override
    {
        return 35;
    }
    void accept(CommandVisitor &v)
    {
        v.visit(shared_from_this());
    }
};

// Rampart magician unit
class Unicorn : public Magician, public std::enable_shared_from_this < Unicorn >
{
public:
    Unicorn(UnitParams pm_) : Magician(pm_) {}
    Unicorn() : Magician(UnitParams("Unicorn", 100, CLOSE, inf)) {}
    virtual ~Unicorn() {}
    virtual inline d_type GetDamage() const override
    {
        return 20 + (rand() % 31);
    }
    void accept(CommandVisitor &v)
    {
        v.visit(shared_from_this());
    }
};

// Rampart great unit
class GreenDragon : public GreatUnit, public std::enable_shared_from_this < GreenDragon >
{
public:
    GreenDragon(UnitParams pm_) : GreatUnit(pm_) {}
    GreenDragon() : GreatUnit(UnitParams("Green Dragon", 500, CLOSE, inf)) {}
    virtual ~GreenDragon() {}
    virtual inline d_type GetDamage() const
    {
        return 135;
    }
    void accept(CommandVisitor &v)
    {
        v.visit(shared_from_this());
    }
};

// This class just stores units
class Army
{
public:
    std::map <int, std::shared_ptr<Unit>> units;
    int last_id;

    Army() : units(), last_id(1) {}
    ~Army() {}
    void AddUnit(std::shared_ptr<Unit> ptr)
    {
        units.insert(std::make_pair(last_id, ptr));
        ptr->SetId(last_id++);
    }
    void DeleteUnit(const int id)
    {
        units.erase(id);
    }
    std::shared_ptr<Unit> operator[](int id)
    {
        if (units.count(id)) {
            return units[id];
        }
        return nullptr;
    }
    // This method will be completely different (or useless) when I implement locations
    std::vector<std::shared_ptr<Unit>> GetNeighbors(const int id)
    {
        std::vector<std::shared_ptr<Unit>> nbr;
        if (units.count(id)) {
            nbr.push_back(units[id]);
        }
        else {
            return nbr;
        }
        auto unit = units.find(id);
        if (unit != units.begin()) {
            --unit;
            if (unit->second->GetHealth() > 0) {
                nbr.push_back(unit->second);
            }
            ++unit;
        }
        if (unit != units.end()) {
            ++unit;
            if (unit != units.end()) {
                if (unit->second->GetHealth() > 0) {
                    nbr.push_back(unit->second);
                }
            }
        }
        return nbr;
    }
    std::map<int, std::shared_ptr<Unit>> GetUnits() const
    {
        return units;
    }
    int AliveNumber() const
    {
        int num = 0;
        for (auto unit : units) {
            num += unit.second->GetHealth() > 0;
        }
        return num;
    }
    int GetKthAlive(int r)
    {
        for (auto unit : units) {
            r -= unit.second->GetHealth() > 0;
            if (r == 0) {
                return unit.first;
            }
        }
        return -1;
    }
    int GetLastId() const
    {
        return last_id;
    }
};

// This class contain an army and can create units to this army. It is the basic class for all cities
class City
{
public:
    City() : army() {}
    virtual ~City() {}
    virtual void CreateUnit(unit_type un_type) = 0;
    Army army;
};

// Implementation of a castle
class Castle : public City
{
public:
    Castle() {}
    virtual ~Castle() {}
    virtual void CreateUnit(unit_type un_type) override
    {
        switch (un_type) {
        case SHOOTING:
            army.AddUnit(std::make_shared<Archery>());
            break;
        case PRANCING:
            army.AddUnit(std::make_shared<Knight>());
            break;
        case MAGICIAN:
            army.AddUnit(std::make_shared<Monk>());
            break;
        case GREAT_UNIT:
            army.AddUnit(std::make_shared<Angel>());
            break;
        default:
            break;
        }
    }
};

// Implementation of a tower
class Tower : public City
{
public:
    Tower() {}
    virtual ~Tower() {}
    virtual void CreateUnit(unit_type un_type) override
    {
        switch (un_type) {
        case SHOOTING:
            army.AddUnit(std::make_shared<MasterGremlin>());
            break;
        case PRANCING:
            army.AddUnit(std::make_shared<Golem>());
            break;
        case MAGICIAN:
            army.AddUnit(std::make_shared<Wizard>());
            break;
        case GREAT_UNIT:
            army.AddUnit(std::make_shared<Titan>());
            break;
        default:
            break;
        }
    }
};

// Implementation of a rampart
class Rampart : public City
{
public:
    Rampart() {}
    virtual ~Rampart() {}
    virtual void CreateUnit(unit_type un_type) override
    {
        switch (un_type) {
        case SHOOTING:
            army.AddUnit(std::make_shared<WoodElf>());
            break;
        case PRANCING:
            army.AddUnit(std::make_shared<Centaur>());
            break;
        case MAGICIAN:
            army.AddUnit(std::make_shared<Unicorn>());
            break;
        case GREAT_UNIT:
            army.AddUnit(std::make_shared<GreenDragon>());
        default:
            break;
        }
    }
};

// This class is basic class for spells.
class Spell
{
    uint mana_nedeed;
    std::string name;
public:
    Spell(uint mana, const std::string &name_) : mana_nedeed(mana), name(name_) {}
    virtual void make_action(class Battle &bt, int hero_id, int unit_id) const = 0;
    uint get_mana() const
    {
        return mana_nedeed;
    }
    std::string get_name() const
    {
        return name;
    }
    virtual ~Spell() {}
};

// Spell class for fireball
class Fireball : public Spell
{
    const uint damage = 20;
public:
    Fireball(uint mana, const std::string &name_) : Spell(mana, name_) {}
    Fireball() : Spell(15, "Fireball") {}
    virtual ~Fireball() {}
    virtual void make_action(class Battle &bt, int hero_id, int unit_id) const override;
};

// Spell class for freeze
class Freeze : public Spell
{
    const uint n_turns = 3;
public:
    Freeze(uint mana, const std::string &name_) : Spell(mana, name_) {}
    Freeze() : Spell(10, "Freeze") {}
    virtual ~Freeze() {}
    virtual void make_action(class Battle &bt, int hero_id, int unit_id) const override;
};

// Spell class for duplicate
class Duplicate : public Spell
{
    const uint n_turns = 3;
public:
    Duplicate(uint mana, const std::string &name_) : Spell(mana, name_) {}
    Duplicate() : Spell(25, "Duplicate") {}
    virtual ~Duplicate() {}
    virtual void make_action(class Battle &bt, int hero_id, int unit_id) const override;
};

// This class outputs all information
class Log
{
public:
    Log() {}
    ~Log() {}
    void PrintMessage(const std::string &message)
    {
        std::cout << message << std::endl;
    }
};

// This is the hero class. It contains it's own army.
class Hero
{
    uint mana;
    uint min_cost;
    std::vector <std::shared_ptr<Spell>> spell_book;
public:
    Army army;
    Hero(Army army_) : mana(MAX_MANA), spell_book(), army(army_)
    {
        spell_book.push_back(std::make_shared<Fireball>());
        spell_book.push_back(std::make_shared<Freeze>());
        spell_book.push_back(std::make_shared<Duplicate>());
        // should be replaced when AddSpell method implemented
        min_cost = 10;
    }
    ~Hero() {}
    bool CastSpell(const std::string &spell_name, class Battle &bt, int hero_id, int unit_id, int target_hero_id);
    void SetMana(uint mana_)
    {
        mana = mana_;
    }
    uint GetMana() const
    {
        return mana;
    }
    uint GetMinCost() const
    {
        return min_cost;
    }
};

// Implementation of taking damage after attack
class SufferDamageVisitor : public CommandVisitor
{
    uint damage;
public:
    SufferDamageVisitor(int d) : damage(d) {}
    virtual ~SufferDamageVisitor() {}

    virtual void visit(std::shared_ptr<class Archery> unit) {
        h_type hl = unit->GetHealth();
        Log log;
        log.PrintMessage("Archery " + std::to_string(unit->GetId()) + " got " + std::to_string(damage) +
            " damages (health left: " + std::to_string(hl >= damage ? hl - damage : 0) + ")");
        if (hl <= damage) {
            unit->SetHealth(0);
            log.PrintMessage("Archery " + std::to_string(unit->GetId()) + " died");
        }
        else {
            unit->SetHealth(hl - damage);
        }
    }
    virtual void visit(std::shared_ptr<class Knight> unit) {
        h_type hl = unit->GetHealth();
        Log log;
        log.PrintMessage("Knight " + std::to_string(unit->GetId()) + " got " + std::to_string(damage) +
            " damages (health left: " + std::to_string(hl >= damage ? hl - damage : 0) + ")");
        if (hl <= damage) {
            unit->SetHealth(0);
            log.PrintMessage("Knight " + std::to_string(unit->GetId()) + " died");
        }
        else {
            unit->SetHealth(hl - damage);
        }
    }
    virtual void visit(std::shared_ptr<class Monk> unit) {
        h_type hl = unit->GetHealth();
        Log log;
        log.PrintMessage("Monk " + std::to_string(unit->GetId()) + " got " + std::to_string(damage) +
            " damages (health left: " + std::to_string(hl >= damage ? hl - damage : 0) + ")");
        if (hl <= damage) {
            unit->SetHealth(0);
            log.PrintMessage("Monk " + std::to_string(unit->GetId()) + " died");
        }
        else {
            unit->SetHealth(hl - damage);
        }
    }
    virtual void visit(std::shared_ptr<class Angel> unit) {
        h_type hl = unit->GetHealth();
        Log log;
        log.PrintMessage("Angel " + std::to_string(unit->GetId()) + " got " + std::to_string(damage) +
            " damages (health left: " + std::to_string(hl >= damage ? hl - damage : 0) + ")");
        if (hl <= damage) {
            unit->SetHealth(0);
            log.PrintMessage("Angel " + std::to_string(unit->GetId()) + " died");
        }
        else {
            unit->SetHealth(hl - damage);
        }
    }
    virtual void visit(std::shared_ptr<class MasterGremlin> unit) {
        h_type hl = unit->GetHealth();
        Log log;
        log.PrintMessage("Master Gremlin " + std::to_string(unit->GetId()) + " got " + std::to_string(damage) +
            " damages (health left: " + std::to_string(hl >= damage ? hl - damage : 0) + ")");
        if (hl <= damage) {
            unit->SetHealth(0);
            log.PrintMessage("Master Gremlin " + std::to_string(unit->GetId()) + " died");
        }
        else {
            unit->SetHealth(hl - damage);
        }
    }
    virtual void visit(std::shared_ptr<class Golem> unit) {
        h_type hl = unit->GetHealth();
        Log log;
        log.PrintMessage("Golem " + std::to_string(unit->GetId()) + " got " + std::to_string(damage) +
            " damages (health left: " + std::to_string(hl >= damage ? hl - damage : 0) + ")");
        if (hl <= damage) {
            unit->SetHealth(0);
            log.PrintMessage("Golem " + std::to_string(unit->GetId()) + " died");
        }
        else {
            unit->SetHealth(hl - damage);
        }
    }
    virtual void visit(std::shared_ptr<class Wizard> unit) {
        h_type hl = unit->GetHealth();
        Log log;
        log.PrintMessage("Wizard " + std::to_string(unit->GetId()) + " got " + std::to_string(damage) +
            " damages (health left: " + std::to_string(hl >= damage ? hl - damage : 0) + ")");
        if (hl <= damage) {
            unit->SetHealth(0);
            log.PrintMessage("Wizard " + std::to_string(unit->GetId()) + " died");
        }
        else {
            unit->SetHealth(hl - damage);
        }
    }
    virtual void visit(std::shared_ptr<class Titan> unit) {
        h_type hl = unit->GetHealth();
        Log log;
        log.PrintMessage("Titan " + std::to_string(unit->GetId()) + " got " + std::to_string(damage) +
            " damages (health left: " + std::to_string(hl >= damage ? hl - damage : 0) + ")");
        if (hl <= damage) {
            unit->SetHealth(0);
            log.PrintMessage("Titan " + std::to_string(unit->GetId()) + " died");
        }
        else {
            unit->SetHealth(hl - damage);
        }
    }
    virtual void visit(std::shared_ptr<class WoodElf> unit) {
        h_type hl = unit->GetHealth();
        Log log;
        log.PrintMessage("Wood Elf " + std::to_string(unit->GetId()) + " got " + std::to_string(damage) +
            " damages (health left: " + std::to_string(hl >= damage ? hl - damage : 0) + ")");
        if (hl <= damage) {
            unit->SetHealth(0);
            log.PrintMessage("Wood Elf " + std::to_string(unit->GetId()) + " died");
        }
        else {
            unit->SetHealth(hl - damage);
        }
    }
    virtual void visit(std::shared_ptr<class Centaur> unit) {
        h_type hl = unit->GetHealth();
        Log log;
        log.PrintMessage("Centaur " + std::to_string(unit->GetId()) + " got " + std::to_string(damage) +
            " damages (health left: " + std::to_string(hl >= damage ? hl - damage : 0) + ")");
        if (hl <= damage) {
            unit->SetHealth(0);
            log.PrintMessage("Centaur " + std::to_string(unit->GetId()) + " died");
        }
        else {
            unit->SetHealth(hl - damage);
        }
    }
    virtual void visit(std::shared_ptr<class Unicorn> unit) {
        h_type hl = unit->GetHealth();
        Log log;
        log.PrintMessage("Unicorn " + std::to_string(unit->GetId()) + " got " + std::to_string(damage) +
            " damages (health left: " + std::to_string(hl >= damage ? hl - damage : 0) + ")");
        if (hl <= damage) {
            unit->SetHealth(0);
            log.PrintMessage("Unicorn " + std::to_string(unit->GetId()) + " died");
        }
        else {
            unit->SetHealth(hl - damage);
        }
    }
    virtual void visit(std::shared_ptr<class GreenDragon> unit) {
        h_type hl = unit->GetHealth();
        Log log;
        log.PrintMessage("Green Dragon " + std::to_string(unit->GetId()) + " got " + std::to_string(damage) +
            " damages (health left: " + std::to_string(hl >= damage ? hl - damage : 0) + ")");
        if (hl <= damage) {
            unit->SetHealth(0);
            log.PrintMessage("Green Dragon " + std::to_string(unit->GetId()) + " died");
        }
        else {
            unit->SetHealth(hl - damage);
        }
    }
};

// Implementation of attack action
class AttackVisitor : public CommandVisitor
{
    std::shared_ptr<Unit> target_unit;
public:
    AttackVisitor(std::shared_ptr<Unit> target_unit_) : target_unit(target_unit_) {}
    virtual ~AttackVisitor() {}

    virtual void visit(std::shared_ptr<class Archery> unit) {
        Log log;
        log.PrintMessage("Archery " + std::to_string(unit->GetId())
            + " attacked unit" + std::to_string(target_unit->GetId()));
        amm_type ammo = unit->GetAmmo();
        if (ammo == 1) {
            unit->SetAmmo(0);
            unit->IncExp();
            SufferDamageVisitor v(unit->GetDamage());
            target_unit->accept(v);
            return;
        }
        if (ammo == 0) {
            return;
        }
        // double strike
        for (int i = 0; i < 2; i++) {
            unit->IncExp();
            unit->SetAmmo(ammo - 1);
            ammo = unit->GetAmmo();
            SufferDamageVisitor v(unit->GetDamage());
            target_unit->accept(v);
        }
    }
    virtual void visit(std::shared_ptr<class Knight> unit) {
        Log log;
        log.PrintMessage("Knight " + std::to_string(unit->GetId())
            + " attacked unit" + std::to_string(target_unit->GetId()));
        unit->IncExp();
        SufferDamageVisitor v(unit->GetDamage());
        target_unit->accept(v);
    }
    virtual void visit(std::shared_ptr<class Monk> unit) {
        Log log;
        log.PrintMessage("Monk " + std::to_string(unit->GetId())
            + " attacked unit" + std::to_string(target_unit->GetId()));
        amm_type ammo = unit->GetAmmo();
        if (ammo == 0) {
            return;
        }
        d_type d_tmp = unit->GetDamage();
        unit->SetAmmo((ammo > d_tmp / 10) ? (ammo - d_tmp / 10) : 0);
        unit->IncExp();
        SufferDamageVisitor v(d_tmp);
        target_unit->accept(v);
    }
    virtual void visit(std::shared_ptr<class Angel> unit) {
        Log log;
        log.PrintMessage("Angel " + std::to_string(unit->GetId())
            + " attacked unit" + std::to_string(target_unit->GetId()));
        unit->IncExp();
        SufferDamageVisitor v(unit->GetDamage());
        target_unit->accept(v);
    }
    virtual void visit(std::shared_ptr<class MasterGremlin> unit) {
        Log log;
        log.PrintMessage("Master Gremlin " + std::to_string(unit->GetId())
            + " attacked unit" + std::to_string(target_unit->GetId()));
        amm_type ammo = unit->GetAmmo();
        if (ammo == 0) {
            return;
        }
        unit->SetAmmo(ammo - 1);
        unit->IncExp();
        SufferDamageVisitor v(unit->GetDamage() + ammo / 2);
        target_unit->accept(v);
    }
    virtual void visit(std::shared_ptr<class Golem> unit) {
        Log log;
        log.PrintMessage("Golem " + std::to_string(unit->GetId())
            + " attacked unit" + std::to_string(target_unit->GetId()));
        unit->IncExp();
        SufferDamageVisitor v(unit->GetDamage());
        target_unit->accept(v);
    }
    virtual void visit(std::shared_ptr<class Wizard> unit) {
        Log log;
        log.PrintMessage("Wizard " + std::to_string(unit->GetId())
            + " attacked unit" + std::to_string(target_unit->GetId()));
        amm_type ammo = unit->GetAmmo();
        if (ammo == 0) {
            return;
        }
        if (ammo < 10) {
            unit->SetAmmo(0);
        }
        else {
            unit->SetAmmo(ammo - 10);
        }
        unit->IncExp();
        SufferDamageVisitor v(unit->GetDamage());
        target_unit->accept(v);
    }
    virtual void visit(std::shared_ptr<class Titan> unit) {
        Log log;
        log.PrintMessage("Titan " + std::to_string(unit->GetId())
            + " attacked unit" + std::to_string(target_unit->GetId()));
        unit->IncExp();
        SufferDamageVisitor v(unit->GetDamage());
        target_unit->accept(v);
    }
    virtual void visit(std::shared_ptr<class WoodElf> unit) {
        Log log;
        log.PrintMessage("Wood Elf " + std::to_string(unit->GetId())
            + " attacked unit" + std::to_string(target_unit->GetId()));
        amm_type ammo = unit->GetAmmo();
        if (ammo == 0) {
            return;
        }
        unit->SetAmmo(ammo - 1);
        unit->IncExp();
        SufferDamageVisitor v(unit->GetDamage() + ammo / 2);
        target_unit->accept(v);
    }
    virtual void visit(std::shared_ptr<class Centaur> unit) {
        Log log;
        log.PrintMessage("Centaur " + std::to_string(unit->GetId())
            + " attacked unit" + std::to_string(target_unit->GetId()));
        unit->IncExp();
        SufferDamageVisitor v(unit->GetDamage());
        target_unit->accept(v);
    }
    virtual void visit(std::shared_ptr<class Unicorn> unit) {
        Log log;
        log.PrintMessage("Unicorn " + std::to_string(unit->GetId())
            + " attacked unit" + std::to_string(target_unit->GetId()));
        unit->IncExp();
        SufferDamageVisitor v(unit->GetDamage());
        target_unit->accept(v);
    }
    virtual void visit(std::shared_ptr<class GreenDragon> unit) {
        Log log;
        log.PrintMessage("Green Dragon " + std::to_string(unit->GetId())
            + " attacked unit" + std::to_string(target_unit->GetId()));
        unit->IncExp();
        SufferDamageVisitor v(unit->GetDamage());
        target_unit->accept(v);
    }
};

// Implementation of clone action
class CloneVisitor : public CommandVisitor
{
public:
    CloneVisitor() {}
    virtual ~CloneVisitor() {}

    std::shared_ptr<Unit> cl;

    virtual void visit(std::shared_ptr<class Archery> unit) {
        cl = std::make_shared<Archery>();
    }
    virtual void visit(std::shared_ptr<class Knight> unit) {
        cl = std::make_shared<Knight>();
    }
    virtual void visit(std::shared_ptr<class Monk> unit) {
        cl = std::make_shared<Monk>();
    }
    virtual void visit(std::shared_ptr<class Angel> unit) {
        cl = std::make_shared<Angel>();
    }
    virtual void visit(std::shared_ptr<class MasterGremlin> unit) {
        cl = std::make_shared<MasterGremlin>();
    }
    virtual void visit(std::shared_ptr<class Golem> unit) {
        cl = std::make_shared<Golem>();
    }
    virtual void visit(std::shared_ptr<class Wizard> unit) {
        cl = std::make_shared<Wizard>();
    }
    virtual void visit(std::shared_ptr<class Titan> unit) {
        cl = std::make_shared<Titan>();
    }
    virtual void visit(std::shared_ptr<class WoodElf> unit) {
        cl = std::make_shared<WoodElf>();
    }
    virtual void visit(std::shared_ptr<class Centaur> unit) {
        cl = std::make_shared<Centaur>();
    }
    virtual void visit(std::shared_ptr<class Unicorn> unit) {
        cl = std::make_shared<Unicorn>();
    }
    virtual void visit(std::shared_ptr<class GreenDragon> unit) {
        cl = std::make_shared<GreenDragon>();
    }
};

// Implementation of duplicate action
class DuplicateVisitor : public CommandVisitor
{
public:
    std::shared_ptr<Unit> dp;
    DuplicateVisitor() {}
    virtual ~DuplicateVisitor() {}

    virtual void visit(std::shared_ptr<class Archery> unit) {
        dp = std::make_shared<Archery>(unit->GetPM());
    }
    virtual void visit(std::shared_ptr<class Knight> unit) {
        dp = std::make_shared<Knight>(unit->GetPM());
    }
    virtual void visit(std::shared_ptr<class Monk> unit) {
        dp = std::make_shared<Monk>(unit->GetPM());
    }
    virtual void visit(std::shared_ptr<class Angel> unit) {
        dp = std::make_shared<Angel>(unit->GetPM());
    }
    virtual void visit(std::shared_ptr<class MasterGremlin> unit) {
        dp = std::make_shared<MasterGremlin>(unit->GetPM());
    }
    virtual void visit(std::shared_ptr<class Golem> unit) {
        dp = std::make_shared<Golem>(unit->GetPM());
    }
    virtual void visit(std::shared_ptr<class Wizard> unit) {
        dp = std::make_shared<Wizard>(unit->GetPM());
    }
    virtual void visit(std::shared_ptr<class Titan> unit) {
        dp = std::make_shared<Titan>(unit->GetPM());
    }
    virtual void visit(std::shared_ptr<class WoodElf> unit) {
        dp = std::make_shared<WoodElf>(unit->GetPM());
    }
    virtual void visit(std::shared_ptr<class Centaur> unit) {
        dp = std::make_shared<Centaur>(unit->GetPM());
    }
    virtual void visit(std::shared_ptr<class Unicorn> unit) {
        dp = std::make_shared<Unicorn>(unit->GetPM());
    }
    virtual void visit(std::shared_ptr<class GreenDragon> unit) {
        dp = std::make_shared<GreenDragon>(unit->GetPM());
    }
};

// This class is for executing user commands
class Command
{
protected:
    class Battle &bt;
    int hero_id;
public:
    command_type type;
    Command(class Battle &bt_, int hero_id_, command_type type_);
    virtual ~Command() {}
    virtual void execute(battle_modes mode) = 0;
};

// Declaration of unit attack command
class UnitAttackCommand : public Command
{
    int unit_id;
public:
    UnitAttackCommand(class Battle &bt_, int hero_id_, int unit_id);
    virtual ~UnitAttackCommand() {}
    virtual void execute(battle_modes mode) override;
};

// Declaration of hero spell command
class HeroSpellCommand : public Command
{
public:
    HeroSpellCommand(class Battle &bt_, int hero_id_);
    virtual ~HeroSpellCommand() {}
    virtual void execute(battle_modes mode) override;
};

// This class is for battle obviously. It operates with heroes who hold armies.
class Battle
{
    std::shared_ptr<Hero> hero[2];
    int timer;
    struct copy {
        int id;
        int cr_time;
        copy(int id_, int cr_time_) : id(id_), cr_time(cr_time_) {}
    };
    std::vector <copy> copies[2];
    std::set <std::pair<std::shared_ptr<Unit>, std::pair<int, int>>> freezed_units;
    void final_check() {
        Log log;
        // delete dead
        for (int i = 0; i < 2; i++) {
            for (auto it = copies[i].begin(); it != copies[i].end();) {
                if (timer - it->cr_time >= 10 || hero[i]->army[it->id]->GetHealth() == 0) {
                    if (timer - it->cr_time && hero[i]->army[it->id]->GetHealth() != 0) {
                        log.PrintMessage("Copy " + std::to_string(it->id) + " vanished");
                    }
                    hero[i]->army.units[it->id]->SetHealth(0);
                    ++it;
                }
                else {
                    ++it;
                }
            }
        }
        // free freezed
        for (auto f_unit = freezed_units.begin(); f_unit != freezed_units.end();) {
            if (timer - f_unit->second.first >= f_unit->second.second) {
                log.PrintMessage(f_unit->first->GetName() + std::to_string(f_unit->first->GetId()) + " unfreezed");
                freezed_units.erase(f_unit++);
            }
            else {
                ++f_unit;
            }
        }
    }

    std::vector <std::shared_ptr<Command>> commands;
    int cur_command;

    std::map <int, std::shared_ptr<Unit>>::iterator it_units[2];
    int cur_army;

    // This function fills full round of commands
    void fill_commands()
    {
        if (it_units[cur_army] == hero[cur_army]->army.units.end()) {
            it_units[cur_army] = hero[cur_army]->army.units.begin();
            if (hero[cur_army]->GetMana() > hero[cur_army]->GetMinCost()) {
                commands.push_back(std::make_shared<HeroSpellCommand>(*this, cur_army));
                cur_army ^= 1;
                return;
            }
        }
        while (it_units[cur_army] != hero[cur_army]->army.units.end() && it_units[cur_army]->second->GetHealth() == 0) {
            ++it_units[cur_army];
        }
        if (it_units[cur_army] != hero[cur_army]->army.units.end()) {
            commands.push_back(std::make_shared<UnitAttackCommand>(*this, cur_army, it_units[cur_army]->first));
            ++it_units[cur_army];
            cur_army ^= 1;
            return;
        }
        else {
            it_units[cur_army] = hero[cur_army]->army.units.begin();
            if (hero[cur_army]->GetMana() > hero[cur_army]->GetMinCost()) {
                commands.push_back(std::make_shared<HeroSpellCommand>(*this, cur_army));
                cur_army ^= 1;
                return;
            }
        }
        while (it_units[cur_army] != hero[cur_army]->army.units.end() && it_units[cur_army]->second->GetHealth() == 0) {
            ++it_units[cur_army];
        }
        commands.push_back(std::make_shared<UnitAttackCommand>(*this, cur_army, it_units[cur_army]->first));
        ++it_units[cur_army];
        cur_army ^= 1;
    }
public:
    Battle(std::shared_ptr<Hero> h1, std::shared_ptr<Hero> h2)
        : hero(), timer(0), copies(), freezed_units(), commands(), cur_command(0), it_units(), cur_army(0)
    {
        hero[0] = h1;
        hero[1] = h2;
        it_units[0] = hero[0]->army.units.begin();
        it_units[1] = hero[1]->army.units.begin();
    }
    ~Battle() {}
    void ProcessBattle(battle_modes mode = HANDLE)
    {
        while (hero[0]->army.AliveNumber() != 0 && hero[1]->army.AliveNumber() != 0) {
            fill_commands();
            ++timer;
            commands[cur_command++]->execute(mode);
            final_check();
        }
        if (hero[0]->army.AliveNumber() != 0) {
            hero[0]->SetMana(MAX_MANA);
            std::cout << "The battle is over. Hero 0 is the winner!\n";
        }
        if (hero[1]->army.AliveNumber() != 0) {
            hero[1]->SetMana(MAX_MANA);
            std::cout << "The battle is over. Hero 1 is the winner!\n";
        }
    }
    void SetFreezed(const std::shared_ptr<Unit> &unit, int n_turns)
    {
        // TODO: freeze more than one time doesn't work correctly..
        freezed_units.insert(std::make_pair(unit, std::make_pair(timer, n_turns)));
    }
    void AddCopy(int id, int army_num)
    {
        copies[army_num].push_back(copy(id, timer));
    }
    std::shared_ptr<Hero> GetHero(int num)
    {
        return hero[num];
    }
    void UndoTurn()
    {
        --timer;
    }
    bool CheckActive(int hero_id, int unit_id)
    {
        if (hero[hero_id]->army[unit_id] == nullptr) {
            return false;
        }
        if (hero[hero_id]->army[unit_id]->GetHealth() == 0) {
            return false;
        }
        for (auto unit : freezed_units) {
            if (unit.first->GetId() == unit_id) {
                return false;
            }
        }
        return true;
    }
};

// Implementation of the hero's cast spell action
bool Hero::CastSpell(const std::string &spell_name, class Battle &bt, int hero_id, int unit_id, int target_hero_id)
{
    Log log;
    auto sp = std::find_if(spell_book.begin(), spell_book.end()
        , [spell_name](std::shared_ptr<Spell> spl) { return spl->get_name() == spell_name; });
    if (sp == spell_book.end()) {
        log.PrintMessage("Unable to cast spell \"" + spell_name + "\": hero don't know this spell");
        return false;
    }
    if ((*sp)->get_mana() > mana) {
        log.PrintMessage("Unable to cast spell \"" + spell_name + "\": hero don't have enough mana");
        return false;
    }
    if (bt.GetHero(target_hero_id)->army[unit_id] == nullptr) {
        log.PrintMessage("Unable to cast spell \"" + spell_name + "\": unit with this id doesn't exist");
        return false;
    }
    if (bt.GetHero(target_hero_id)->army[unit_id]->GetHealth() == 0) {
        log.PrintMessage("Unable to cast spell \"" + spell_name + "\": unit with this id is dead");
    }
    mana -= (*sp)->get_mana();
    log.PrintMessage("Successfully casted spell \"" + spell_name + "\" to unit " + std::to_string(unit_id)
        + "(mana left: " + std::to_string(mana) + ")");
    (*sp)->make_action(bt, target_hero_id, unit_id);
    return true;
}

// Constructor for unit attack command
UnitAttackCommand::UnitAttackCommand(class Battle &bt_, int hero_id_, int unit_id_)
    : Command(bt_, hero_id_, UNIT_ATTACK), unit_id(unit_id_)
{
}

// Implementation of unit attack command
void UnitAttackCommand::execute(battle_modes mode)
{
    // perhaps it is not the most convenient way.. Here we ignore empty commands
    if (bt.CheckActive(hero_id, unit_id) == false) {
        bt.UndoTurn();
        return;
    }
    while (true) {
        int target_id;
        char c;
        std::shared_ptr<Unit> target_unit, unit;
        auto e_hero = bt.GetHero(hero_id ^ 1);
        auto hero = bt.GetHero(hero_id);
        Log log;

        unit = hero->army[unit_id];
        if (mode == HANDLE) {
            std::cout << "Enter " << unit->GetName() << unit_id << " from army " << hero_id << " action\n";
            std::cin >> c;
            if (c == '.') {
                log.PrintMessage(unit->GetName() + std::to_string(unit_id) + " has done nothing");
                return;
            }
            std::cin >> target_id;
        }
        if (mode == AUTOMATIC) {
            int r = (rand() % e_hero->army.AliveNumber()) + 1;
            target_id = e_hero->army.GetKthAlive(r);
            c = 'A';
        }

        target_unit = e_hero->army[target_id];
        AttackVisitor v(target_unit);

        switch (c) {
        case 'A':
            if (target_unit == nullptr) {
                log.PrintMessage("Can't attack unit "
                    + std::to_string(target_id) + ": unit with this id doesn't exist");
                break;
            }
            if (target_unit->GetHealth() == 0) {
                log.PrintMessage("Can't attack unit "
                    + std::to_string(target_id) + ": unit with this id is dead");
                break;
            }
            unit->accept(v);
            return;
        default:
            log.PrintMessage("Wrong command");
            break;
        }
    }
}

// Constructor for hero spell command
HeroSpellCommand::HeroSpellCommand(Battle &bt_, int hero_id_)
    : Command(bt_, hero_id_, HERO_SPELL)
{
}

// Implementation of hero spell command
void HeroSpellCommand::execute(battle_modes mode)
{
    // perhaps it is not the most convenient way.. Here we ignore empty comands
    if (bt.GetHero(hero_id)->GetMana() < bt.GetHero(hero_id)->GetMinCost()) {
        bt.UndoTurn();
        return;
    }
    while (true) {
        char c;
        int target_id;
        Log log;
        if (mode == HANDLE) {
            std::cout << "Enter hero " << hero_id << " action\n";
            std::cin >> c;
            if (c == '.') {
                log.PrintMessage("Hero " + std::to_string(hero_id) + " has done nothing");
                return;
            }
            std::cin >> target_id;
        }
        if (mode == AUTOMATIC) {
            const std::string tmp = "DFB";
            int r = rand() % 3;
            c = tmp[r];
        }
        auto hero = bt.GetHero(hero_id);
        auto e_hero = bt.GetHero(hero_id ^ 1);
        switch (c) {
        case 'D':
            if (mode == AUTOMATIC) {
                int r = (rand() % hero->army.AliveNumber()) + 1;
                target_id = hero->army.GetKthAlive(r);
            }
            if (hero->CastSpell("Duplicate", bt, hero_id, target_id, hero_id)) {
                return;
            }
            break;
        case 'F':
            if (mode == AUTOMATIC) {
                int r = (rand() % e_hero->army.AliveNumber()) + 1;
                target_id = e_hero->army.GetKthAlive(r);
            }
            if (hero->CastSpell("Freeze", bt, hero_id, target_id, hero_id ^ 1)) {
                return;
            }
            break;
        case 'B':
            if (mode == AUTOMATIC) {
                int r = (rand() % e_hero->army.AliveNumber()) + 1;
                target_id = e_hero->army.GetKthAlive(r);
            }
            if (hero->CastSpell("Fireball", bt, hero_id, target_id, hero_id ^ 1)) {
                return;
            }
            break;
        default:
            log.PrintMessage("Wrong command");
            break;
        }
    }
}

// Constructor for basic command class
Command::Command(class Battle &bt_, int hero_id_, command_type type_)
    : bt(bt_), hero_id(hero_id_), type(type_)
{
}

// Implementation of fireball spell
void Fireball::make_action(class Battle &bt, int hero_id, int unit_id) const
{
    auto hero = bt.GetHero(hero_id);
    auto nbr = hero->army.GetNeighbors(unit_id);
    for (auto unit : nbr) {
        SufferDamageVisitor v(damage);
        unit->accept(v);
    }
}

//Implementation of freeze spell
void Freeze::make_action(class Battle &bt, int hero_id, int unit_id) const
{
    auto hero = bt.GetHero(hero_id);
    bt.SetFreezed(hero->army[unit_id], n_turns);
}

// Implementation of duplicate spell
void Duplicate::make_action(class Battle &bt, int hero_id, int unit_id) const
{
    auto hero = bt.GetHero(hero_id);
    DuplicateVisitor v;
    hero->army[unit_id]->accept(v);
    std::shared_ptr<Unit> ut = v.dp;
    hero->army.AddUnit(ut);
    bt.AddCopy(ut->GetId(), hero_id);
}

// This class implements a game process. It counts time and do the tasks. 
class Game
{
    std::shared_ptr<City> city[2];
    Game(const Game &) = delete;
    Game & operator =(const Game &) = delete;
public:
    Game() : city()
    {
        srand(time(NULL));
        int tp[2];
        std::string msg[2];
        msg[0] = "Enter first hero's city (between 1 and 3)\n";
        msg[1] = "Enter second hero's city (between 1 and 3)\n";
        for (int i = 0; i < 2; i++) {
            while (true) {
                std::cout << msg[i];
                std::cin >> tp[i];
                if (tp[i] < 1 || tp[i] > 3) {
                    std::cout << "Wrong number\n";
                }
                else {
                    break;
                }
            }
            switch (tp[i]) {
            case CASTLE:
                city[i] = std::make_shared<Castle>();
                std::cout << "Ok, the city is \"Castle\"\n";
                break;
            case TOWER:
                city[i] = std::make_shared<Tower>();
                std::cout << "Ok, the city is \"Tower\"\n";
                break;
            case RAMPART:
                city[i] = std::make_shared<Rampart>();
                std::cout << "Ok, the city is \"Rampart\"\n";
                break;
            default:
                break;
            }
        }
        msg[0] = "Enter first hero's army (between 1 and 4, finish -- 0)\n";
        msg[1] = "Enter second hero's army (between 1 and 4, finish -- 0)\n";
        for (int i = 0; i < 2; i++) {
            std::cout << msg[i];
            int t;
            while (true) {
                std::cin >> t;
                if (t == 0) {
                    break;
                }
                if (t < 1 || t > 4) {
                    std::cout << "Wrong number\n";
                    continue;
                }
                city[i]->CreateUnit(unit_type(t));
                std::cout << "Ok, unit created\n";
            }
        }
    }
    ~Game(){}
    void DoWork()
    {
        std::shared_ptr<Hero> hero[2];
        for (int i = 0; i < 2; i++) {
            hero[i] = std::make_shared<Hero>(city[i]->army);
        }
        Battle bt(hero[0], hero[1]);
        int t;
        while (true) {
            std::cout << "Enter battle mode (1 -- handle, 2 -- automatic)\n";
            std::cin >> t;
            if (t < 1 || t > 2) {
                std::cout << "Wrong number\n";
            }
            else {
                break;
            }
        }
        bt.ProcessBattle(battle_modes(t));
    }
};

int main()
{
    Game hmm;
    hmm.DoWork();
    return 0;
}