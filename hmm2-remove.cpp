#include <iostream>
#include <cstdio>
#include <vector>
#include <string>
#include <ctime>
#include <memory>

typedef unsigned int uint;
typedef uint h_type;
typedef uint d_type;
typedef uint amm_type;
typedef uint exp_type;
#define inf -1

enum town_type {
    CASTLE = 1,
    TOWER = 2
};

enum unit_type {
    SHOOTING = 1,
    PRANCING = 2,
    MAGICIAN = 3
};

enum attack_type {
    DISTANCE,
    CLOSE
};

class Unit
{
protected:
    struct UnitParams {
        std::string name;
        h_type health;
        attack_type attack;
        amm_type ammunition;
        exp_type exp;
        UnitParams(std::string n, h_type h, attack_type a, amm_type amm) :
            name(n), health(h), attack(a), ammunition(amm), exp(0) {}
    };
    UnitParams pm;
    int id;

    virtual inline d_type GetDamage() const
    {
        return 10;
    }
public:
    Unit(UnitParams pm_) : pm(pm_), id(0) {}
    virtual ~Unit() {}
    h_type GetHealth() const
    {
        return pm.health;
    }
    virtual d_type Attack()
    {
        ++pm.exp;
        return GetDamage();
    }
    std::string ToString() const
    {
        return pm.name;
    }
    virtual std::shared_ptr<Unit> Clone() const = 0;
    virtual std::shared_ptr<Unit> Duplicate() const = 0;
    int GetId() const
    {
        return id;
    }
    void SetId(int id_)
    {
        id = id_;
    }
};

class Shooting : public Unit
{
public:
    Shooting(UnitParams pm_) : Unit(pm_) {}
    virtual ~Shooting() {}
};

class Prancing : public Unit
{
public:
    Prancing(UnitParams pm_) : Unit(pm_) {}
    virtual ~Prancing() {}
};

class Magician : public Unit
{
public:
    Magician(UnitParams pm_) : Unit(pm_) {}
    virtual ~Magician() {}
};

class Archery : public Shooting
{
    virtual inline d_type GetDamage() const
    {
        return 10;
    }
public:
    Archery(UnitParams pm_) : Shooting(pm_) {}
    Archery() : Shooting(UnitParams("Archery", 45, DISTANCE, 15)) {}
    virtual ~Archery() {}
    virtual d_type Attack()
    {
        if (pm.ammunition == 1) {
            pm.ammunition = 0;
            return GetDamage();
        }
        if (pm.ammunition == 0) {
            return 0;
        }
        ++pm.exp;
        pm.ammunition -= 2;
        return GetDamage() * 2;
    }
    virtual std::shared_ptr<Unit> Clone() const
    {
        return std::make_shared<Archery>();
    }
    virtual std::shared_ptr<Unit> Duplicate() const
    {
        return std::make_shared<Archery>(pm);
    }
};

class Knight : public Prancing
{
    virtual inline d_type GetDamage() const
    {
        return 20;
    }
public:
    Knight(UnitParams pm_) : Prancing(pm_) {}
    Knight() : Prancing(UnitParams("Knight", 145, CLOSE, inf)) {}
    virtual ~Knight() {}
    virtual std::shared_ptr<Unit> Clone() const
    {
        return std::make_shared<Knight>();
    }
    virtual std::shared_ptr<Unit> Duplicate() const
    {
        return std::make_shared<Knight>(pm);
    }
};

class Monk : public Magician
{
    virtual inline d_type GetDamage() const
    {
        return 20 + (rand() % 11);
    }
public:
    Monk(UnitParams pm_) : Magician(pm_) {}
    Monk() : Magician(UnitParams("Monk", 100, DISTANCE, 30)) {}
    virtual ~Monk() {}
    virtual d_type Attack()
    {
        if (pm.ammunition == 0) {
            return 0;
        }
        d_type d_tmp = GetDamage();
        pm.ammunition = (pm.ammunition > d_tmp / 10) ? (pm.ammunition - d_tmp / 10) : 0;
        ++pm.exp;
        return d_tmp;
    }
    virtual std::shared_ptr<Unit> Clone() const
    {
        return std::make_shared<Monk>();
    }
    virtual std::shared_ptr<Unit> Duplicate() const
    {
        return std::make_shared<Monk>(pm);
    }
};

class MasterGremlin : public Shooting
{
    virtual inline d_type GetDamage() const
    {
        return 15;
    }
public:
    MasterGremlin(UnitParams pm_) : Shooting(pm_) {}
    MasterGremlin() : Shooting(UnitParams("MasterGremlin", 25, DISTANCE, 10)) {}
    virtual ~MasterGremlin() {}
    virtual d_type Attack()
    {
        if (pm.ammunition == 0) {
            return 0;
        }
        --pm.ammunition;
        ++pm.exp;
        return GetDamage() + pm.ammunition / 2;
    }
    virtual std::shared_ptr<Unit> Clone() const
    {
        return std::make_shared<MasterGremlin>();
    }
    virtual std::shared_ptr<Unit> Duplicate() const
    {
        return std::make_shared<MasterGremlin>(pm);
    }
};

class Golem : public Prancing
{
    virtual inline d_type GetDamage() const
    {
        return 30;
    }
public:
    Golem(UnitParams pm_) : Prancing(pm_) {}
    Golem() : Prancing(UnitParams("Golem", 175, CLOSE, inf)) {}
    virtual ~Golem() {}
    virtual std::shared_ptr<Unit> Clone() const
    {
        return std::make_shared<Golem>();
    }
    virtual std::shared_ptr<Unit> Duplicate() const
    {
        return std::make_shared<Golem>(pm);
    }
};

class Wizard : public Magician
{
    virtual inline d_type GetDamage() const
    {
        return 30 + (rand() % 21);
    }
public:
    Wizard(UnitParams pm_) : Magician(pm_) {}
    Wizard() : Magician(UnitParams("Wizard", 90, DISTANCE, 50)) {}
    virtual ~Wizard() {}
    virtual d_type Attack()
    {
        if (pm.ammunition == 0) {
            return 0;
        }
        if (pm.ammunition < 10) {
            pm.ammunition = 0;
        }
        else {
            pm.ammunition -= 10;
        }
        ++pm.exp;
        return GetDamage();
    }
    virtual std::shared_ptr<Unit> Clone() const
    {
        return std::make_shared<Wizard>();
    }
    virtual std::shared_ptr<Unit> Duplicate() const
    {
        return std::make_shared<Wizard>(pm);
    }
};

class Army
{
    std::vector <std::shared_ptr<Unit>> units;
    int last_id;
public:
    Army() : units(), last_id(1) {}
    ~Army()
    {
    }
    void AddUnit(std::shared_ptr<Unit> ptr)
    {
        units.push_back(ptr);
        ptr->SetId(last_id++);
    }
    void DeleteUnit(const int id)
    {
        for (auto it = units.begin(); it != units.end(); ++it) {
            if ((*it)->GetId() == id) {
                units.erase(it);
                break;
            }
        }
    }
    std::shared_ptr<Unit> operator[](const int id)
    {
        for (auto it = units.begin(); it != units.end(); ++it) {
            if ((*it)->GetId() == id) {
                return *it;
            }
        }
        return nullptr;
    }
};

class City
{
public:
    City() : army() {}
    virtual ~City() {}
    virtual void CreateUnit(unit_type un_type) = 0;
    Army army;
};

class Castle : public City
{
public:
    Castle() {}
    virtual ~Castle() {}
    virtual void CreateUnit(unit_type un_type)
    {
        switch (un_type) {
        case SHOOTING:
            army.AddUnit(std::make_shared<Archery>());
            break;
        case PRANCING:
            army.AddUnit(std::make_shared<Knight>());
            break;
        case MAGICIAN:
            army.AddUnit(std::make_shared<Monk>());
            break;
        default:
            break;
        }
    }
};

class Tower : public City
{
public:
    Tower() {}
    virtual ~Tower() {}
    virtual void CreateUnit(unit_type un_type)
    {
        switch (un_type) {
        case SHOOTING:
            army.AddUnit(std::make_shared<MasterGremlin>());
            break;
        case PRANCING:
            army.AddUnit(std::make_shared<Golem>());
            break;
        case MAGICIAN:
            army.AddUnit(std::make_shared<Wizard>());
            break;
        default:
            break;
        }
    }
};

class Game {
    std::shared_ptr<City> city;
    Game(const Game &) = delete;
    Game & operator =(const Game &) = delete;
    int timer;
    struct copy {
        int id;
        int cr_time;
        copy(int id_, int cr_time_) : id(id_), cr_time(cr_time_) {}
    };
    std::vector <copy> copies;
    void delete_doubles() {
        for (auto it = copies.begin(); it != copies.end();) {
            if (timer - it->cr_time >= 10) {
                city->army.DeleteUnit(it->id);
                std::cout << "- " << it->id << std::endl;
                it = copies.erase(it);
            }
            else {
                ++it;
            }
        }
    }
public:
    Game() : city(), timer(0), copies()
    {
        srand(time(NULL));
        int tp;
        std::cin >> tp;
        if (tp == CASTLE) {
            city = std::make_shared<Castle>();
        }
        if (tp == TOWER) {
            city = std::make_shared<Tower>();
        }
    }
    ~Game(){}
    void DoWork()
    {
        int tp;
        while (std::cin >> tp) {
            city->CreateUnit(unit_type(tp));
        }
        std::cin.clear();
        char c;
        while (std::cin >> c) {
            ++timer;
            if (c == '.') {
                delete_doubles();
                continue;
            }
            std::cin >> tp;
            if (c == 'A') {
                city->army[tp]->Attack();
            }
            if (c == 'C') {
                std::shared_ptr <Unit> ut = city->army[tp]->Clone();
                city->army.AddUnit(ut);
                std::cout << ut->GetId() << std::endl;
            }
            if (c == 'D') {
                std::shared_ptr <Unit> ut = city->army[tp]->Duplicate();
                city->army.AddUnit(ut);
                std::cout << ut->GetId() << std::endl;
                copies.push_back(copy(ut->GetId(), timer));
            }
            delete_doubles();
        }
    }
};

int main()
{
    Game hmm;
    hmm.DoWork();
    return 0;
}