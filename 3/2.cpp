#include <iostream>
using namespace std;

class C
{
    int c;
public:
    C(int c_) : c(c_) {}
    C(const C &a) : c(0) {
        c = a.c;
    }
    C() : c(0) {}
    C operator +(const C &a) {
        C b;
        b.c = (c + a.c) * 2;
        return b;
    }
    int get() {
        return c * 2;
    }
};
