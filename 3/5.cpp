#include <iostream>
#include <cstdio>
#include <string>
#include <string.h>
using namespace std;

struct StringBuf
{
    char *buf;
    int num;
    StringBuf() : buf(nullptr), num(0) {}
    StringBuf(const char *s) : buf(nullptr), num(1) 
    {
        buf = new char[strlen(s) + 1];
        strcpy(buf, s);
    }
    ~StringBuf() 
    {
        if (buf != nullptr) {
            delete[] buf;
        }
    }
    StringBuf *clone()
    {
        StringBuf *n_buf = new StringBuf(buf);
        return n_buf;
    }
};

class String
{
    StringBuf *impl;
    void release();
    void addRef();
    class CharWrap
    {
        int num;
        String * str;
    public:
        CharWrap(const int num_, String * str_) : num(num_), str(str_) {} 
        CharWrap & operator =(const char c)
        {
            str->changeNum(num, c);
            return *this;
        }
        operator char() const
        {
            return str->getNum(num);
        }
    };
public:
    String();
    String(const char *s);
    String(const String &s);
    operator string() const;
    String & operator =(const String &s);
    String operator +=(const char *s);
    CharWrap operator [](const int num);
    char operator [](const int num) const;
    char getNum(int num) const;
    void changeNum(int num, char c);
    ~String();
};

String::String() : impl(nullptr)
{

}

String::String( const char *s ) : impl(nullptr)
{
    impl = new StringBuf(s);
}

String::String( const String &s ) : impl(nullptr)
{
    impl = s.impl;
    addRef();
}

char String::operator [](const int num) const
{
    return impl->buf[num];
}

void String::release()
{
    if (impl == nullptr) {
        return;
    }
    --impl->num;
    if (impl->num <= 0) {
        delete impl;
    }
}

void String::addRef()
{
    ++impl->num;
}

char String::getNum(int num) const
{
    return impl->buf[num];
}

void String::changeNum(int num, char c)
{
    if (c == impl->buf[num]) {
        return;
    }
    StringBuf *tmp = impl->clone();
    release();
    impl = tmp;
    impl->buf[num] = c;
}

String::operator string() const
{
    return string(impl->buf);
}

String & String::operator=( const String &s )
{
    if (this == &s) {
        return *this;
    }
    release();
    impl = s.impl;
    addRef();
    return *this;
}

String String::operator+=( const char *s )
{
    char *tmp = new char[strlen(impl->buf) + strlen(s) + 1];
    strcpy(tmp, impl->buf);
    strcat(tmp, s);
    release();
    impl = new StringBuf(tmp);
    delete[] tmp;
    return *this;
}

String::CharWrap String::operator[]( const int num )
{
    return CharWrap(num, this);
}

String::~String()
{
    release();
}