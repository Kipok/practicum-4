#include <iostream>
#include <cmath>
#include <algorithm>
using namespace std;

#define PI (4.0 * atan(1.0))
#define EPS 1e-9
#define SIZE 10

double f1(double x)
{
    return sin(x);
}

double f2(double x)
{
    return cos(x);
}

double f3(double x)
{
    return x * x;
}

double f4(double x)
{
    return fabs(x);
}

double f5(double x)
{
    return exp(x);
}

double f6(double x)
{
    return sqrt(42 + x * x);
}

double f7(double x)
{
    return x * x * x;
}

double f8(double x)
{
    return cos(x + PI / 3.0);
}

double f9(double x)
{
    return sin(x - PI / 42);
}

double f10(double x)
{
    return log(2 + x * x);
}

class Func 
{
public:
    double (*f)(double);
    int func_num;
    Func(double (*f_)(double), int func_num_) : f(f_), func_num(func_num_) {}
};

class my_comp
{
    double y;
    int num;
public:
    my_comp(double y_, int num_) : y(y_), num(num_) {}
    bool operator() (const Func &a, const Func &b);
};



bool my_comp::operator()(const Func &a, const Func &b)
{
    if (num == 1) {
        return ( a.f(y) + EPS < b.f(y) ) ||
        ( fabs(a.f(y) - b.f(y)) < EPS && a.func_num < b.func_num );
    } else {
        return ( fabs(a.f(y)) + EPS < fabs(b.f(y)) ) ||
        ( fabs(a.f(y) - b.f(y)) < EPS && a.func_num > b.func_num );
    }
}

int main()
{
    int num;
    double y, z;
    cin >> num >> y >> z;
    Func f_arr[] = {Func(f1, 1), Func(f2, 2), 
                    Func(f3, 3), Func(f4, 4), 
                    Func(f5, 5), Func(f6, 6), 
                    Func(f7, 7), Func(f8, 8), 
                    Func(f9, 9), Func(f10, 10)};
    my_comp cmp(y, num);
    sort(f_arr, f_arr + SIZE, cmp);
    for (int i = SIZE - 1; i >= 0; i--) {
        z = f_arr[i].f(z);
    }
    cout.setf(ios::fixed);
    cout.precision(6);

    cout << z << endl;
    return 0;
}