#include <iostream>
#include <cstdio>
#include <vector>
#include <string>
#include <memory.h>
#include <cstdlib>
using namespace std;

#define toInt(c) (c - 'A')

inline bool isNonTerminal(char c)
{
    return c >= 'A' && c <= 'Z';
}

inline bool isTerminal(char c)
{
    return c >= 'a' && c <= 'z';
}

class FixGrammar
{
    vector <string> rp;
    vector <char> lp;
    bool u[26];
    vector<bool> ul;
    bool unr[26];
    bool g[26][26];
    void dfs(int x)
    {
        unr[x] = true;
        for (int i = 0; i < 26; i++) {
            if (g[x][i] && !unr[i]) {
                dfs(i);
            }
        }
    }
public:
    FixGrammar() : rp(), lp(), ul()
    {
        memset(u, 0, sizeof(u));
        memset(unr, 0, sizeof(unr));
        memset(g, 0, sizeof(g));
        char c;
        string s;
        while (cin >> c >> s) {
            lp.push_back(c);
            rp.push_back(s);
            for (unsigned int j = 0; j < s.size(); j++) {
                if (isNonTerminal(s[j])) {
                    g[toInt(c)][toInt(s[j])] = true;
                }
            }
            unsigned int i = 0;
            for (; i < s.size(); i++) {
                if (!isTerminal(s[i])) {
                    break;
                }
            }
            if (i == s.size()) {
                u[toInt(c)] = true;
                ul.push_back(true);
            }
            else {
                ul.push_back(false);
            }
        }
    }
    void delete_unreachable()
    {
        dfs(toInt('S'));
    }
    void delete_vain() 
    {
        bool f = true;
        while (f) {
            f = false;
            for (unsigned int i = 0; i < lp.size(); i++) {
                if (!ul[i]) {
                    unsigned int j = 0;
                    for (; j < rp[i].size(); j++) {
                        if (isNonTerminal(rp[i][j]) && !u[toInt(rp[i][j])]) {
                            break;
                        }
                    }
                    if (j == rp[i].size()) {
                        ul[i] = true;
                        f = true;
                        u[toInt(lp[i])] = true;
                    }
                }
            }
        }
    }
    void print_grammar()
    {
        for (unsigned int i = 0; i < lp.size(); i++) {
            if (ul[i] && unr[toInt(lp[i])]) {
                cout << lp[i] << ' ' << rp[i] << endl;
            }
        }
    }
};


int main()
{
    FixGrammar g;
    g.delete_vain();
    g.delete_unreachable();
    g.print_grammar();
    return 0;
}