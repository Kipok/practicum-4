#include <iostream>
#include <cstdio>
#include <vector>
#include <string>
#include <memory.h>
#include <cstdlib>
#include <algorithm>
using namespace std;

#define toInt(c) (c - 'A')

inline bool isNonTerminal(char c)
{
    return c >= 'A' && c <= 'Z';
}

inline bool isTerminal(char c)
{
    return (c >= 'a' && c <= 'z') || (c >= '0' && c <= '9');
}

class FixGrammar
{
    struct Rule {
        char lp;
        string rp;
        bool operator <(const Rule &r) const {
            return lp < r.lp || (lp == r.lp && rp < r.rp);
        }
        bool operator ==(const Rule &r) const {
            return lp == r.lp && rp == r.rp;
        }
        Rule() : lp(0), rp() {}
        Rule(char lp_, string rp_) : lp(lp_), rp(rp_) {}
    };
    vector <Rule> rules;
    bool u[26];
    vector<bool> ul;
    bool g[26][26];
    void dfs(int x)
    {
        u[x] = true;
        for (int i = 0; i < 26; i++) {
            if (g[x][i] && !u[i]) {
                dfs(i);
            }
        }
    }
    void mark_epsilon()
    {
        memset(u, 0, sizeof(u));
        ul.clear();
        int d = rules.size();
        ul.resize(rules.size());
        for (int i = 0; i < d; i++) {
            if (rules[i].rp == "_") {
                ul[i] = true;
                u[toInt(rules[i].lp)] = true;
            }
            else {
                ul[i] = false;
            }
        }
        bool f = true;
        while(f == true) {
            f = false;
            for (unsigned int i = 0; i < rules.size(); i++) {
                unsigned int j = 0;
                if (u[toInt(rules[i].lp)] == true) {
                    continue;
                }
                for (; j < rules[i].rp.size(); j++) {
                    if (isTerminal(rules[i].rp[j]) || !u[toInt(rules[i].rp[j])]) {
                        break;
                    }
                }
                if (j == rules[i].rp.size()) {
                    u[toInt(rules[i].lp)] = true;
                    f = true;
                }
            }
        }
    }
public:
    FixGrammar() : rules(), ul()
    {
        char c;
        string s;
        while (cin >> c >> s) {
            add_rule(c, s);
        }
    }
    void add_rule(const char c, const string &s) 
    {
        rules.push_back(Rule(c, s));
    }
    void delete_unreachable()
    {
        memset(u, 0, sizeof(u));
        memset(g, 0, sizeof(g));
        for (unsigned int i = 0; i < rules.size(); i++) {
            for (unsigned int j = 0; j < rules[i].rp.size(); j++) {
                if (isNonTerminal(rules[i].rp[j])) {
                    g[toInt(rules[i].lp)][toInt(rules[i].rp[j])] = true;
                }
            }
        }
        dfs(toInt('S'));
        for (int i = rules.size() - 1; i >= 0; i--) {
            if ( u[toInt(rules[i].lp)] == false ) {
                rules.erase(rules.begin() + i);
            }
        }
    }
    void delete_vain()
    {
        ul.clear();
        memset(u, 0, sizeof(u));
        for (unsigned int j = 0; j < rules.size(); j++) {
            unsigned int i = 0;
            for (; i < rules[j].rp.size(); i++) {
                if (!isTerminal(rules[j].rp[i])) {
                    break;
                }
            }
            if (i == rules[j].rp.size()) {
                u[toInt(rules[j].lp)] = true;
                ul.push_back(true);
            }
            else {
                ul.push_back(false);
            }
        }

        bool f = true;
        while (f) {
            f = false;
            for (unsigned int i = 0; i < rules.size(); i++) {
                if (!ul[i]) {
                    unsigned int j = 0;
                    for (; j < rules[i].rp.size(); j++) {
                        if (isNonTerminal(rules[i].rp[j]) && !u[toInt(rules[i].rp[j])]) {
                            break;
                        }
                    }
                    if (j == rules[i].rp.size()) {
                        ul[i] = true;
                        f = true;
                        u[toInt(rules[i].lp)] = true;
                    }
                }
            }
        }
        for (int i = rules.size() - 1; i >= 0; i--) {
            if (ul[i] == false) {
                rules.erase(rules.begin() + i);
            }
        }
    }
    void print_grammar()
    {
        for (unsigned int i = 0; i < rules.size(); i++) {
            cout << rules[i].lp << ' ' << rules[i].rp << endl;
        }
    }
    void delete_equal() 
    {
        sort(rules.begin(), rules.end());
        rules.erase(unique(rules.begin(), rules.end()), rules.end());
    }
    void delete_epsilon()
    {
        mark_epsilon();
        for (int i = rules.size() - 1; i >= 0; i--) {
            if (ul[i] == true) {
                rules.erase(rules.begin() + i);
            }
        }
        int d = rules.size();
        for (int i = 0; i < d; i++) {
            int count = 0;
            for (unsigned int j = 0; j < rules[i].rp.size(); j++) {
                if (isNonTerminal(rules[i].rp[j]) && u[toInt(rules[i].rp[j])]) {
                    ++count;
                }
            }
            for (long long mask = 0; mask < (1ll << count) - 1; mask++) {
                int t = 0;
                string s = "";
                for (unsigned int j = 0; j < rules[i].rp.size(); j++) {
                    if (isNonTerminal(rules[i].rp[j]) && u[toInt(rules[i].rp[j])]) {
                        if (mask & (1ll << t)) {
                            s.push_back(rules[i].rp[j]);
                        }
                        ++t;
                    }
                    else {
                        s.push_back(rules[i].rp[j]);
                    }
                }
                if (s != "" && !(s.size() == 1 && rules[i].lp == s[0])) {
                    add_rule(rules[i].lp, s);
                }

            }
        }
        if (u[toInt('S')]) {
            add_rule('@', "S");
            add_rule('@', "_");
        }
        delete_equal();
        delete_vain();
        delete_unreachable();
    }
};


int main()
{
    FixGrammar g;
    g.delete_epsilon();
    g.print_grammar();
    return 0;
}