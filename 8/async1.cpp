#include <iostream>
#include <cstdio>
#include <string>
#include <algorithm>
#include <map>
#include <fstream>
#include <future>
#include <functional>
#include <thread>
using namespace std;

void
process_words(const string &filename, map <string, int> &m)
{
    ifstream fin(filename);
    string s;
    while (fin >> s) {
        if (m.count(s) == 0) {
            m.insert(make_pair(s, 1));
        }
        else {
            ++m[s];
        }
    }
}

int main(int argc, char *argv[])
{
    map <string, int> whole;
    vector <map <string, int>> cur_maps(argc);
    vector <future <void>> futs(argc);
    for (int i = 1; i < argc; i++) {
        futs[i] = async(launch::async | launch::deferred, process_words, argv[i], ref(cur_maps[i]));
    }
    for (int i = 1; i < argc; i++) {
        futs[i].get();
        for (auto it = cur_maps[i].begin(); it != cur_maps[i].end(); ++it) {
            if (whole.count(it->first) == 0) {
                whole.insert(make_pair(it->first, it->second));
            }
            else {
                whole[it->first] += it->second;
            }
        }
    }
    int ans = 0;
    vector <pair <int, string>> vec(whole.size());
    int j = 0;
    for (auto it = whole.begin(); it != whole.end(); ++it) {
        ++ans;
        vec[j++] = make_pair(it->second, it->first);
    }
    sort(vec.begin(), vec.end());
    cout << ans << endl;
    j = 0;
    for (auto it = vec.rbegin(); it != vec.rend() && j < 20; ++it, ++j) {
        cout << it->second << ' ' << it->first << endl;
    }
    return 0;
}