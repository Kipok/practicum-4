#include <iostream>
#include <cstdio>
#include <vector>
#include <string>
#include <memory.h>
#include <cstdlib>
using namespace std;

#define toInt(c) (c - 'A')

inline bool isNonTerminal(char c)
{
    return c >= 'A' && c <= 'Z';
}

class FixGrammar
{
    vector <string> rp;
    vector <char> lp;
    bool g[26][26];
    bool u[26];
    void dfs(int x)
    {
        u[x] = true;
        for (int i = 0; i < 26; i++) {
            if (g[x][i] && !u[i]) {
                dfs(i);
            }
        }
    }
public:
    FixGrammar() : rp(), lp()
    {
        memset(u, 0, sizeof(u));
        memset(g, 0, sizeof(g));
        char c;
        string s;
        while (cin >> c >> s) {
            lp.push_back(c);
            rp.push_back(s);
            for (unsigned int i = 0; i < s.size(); i++) {
                if (isNonTerminal(s[i])) {
                    g[toInt(c)][toInt(s[i])] = true;
                }
            }
        }
    }
    void fix() 
    {
        dfs(toInt('S'));
    }
    void print_grammar()
    {
        for (unsigned int i = 0; i < lp.size(); i++) {
            if (u[toInt(lp[i])]) {
                cout << lp[i] << ' ' << rp[i] << endl;
            }
        }
    }
};


int main()
{
    FixGrammar g;
    g.fix();
    g.print_grammar();
    return 0;
}