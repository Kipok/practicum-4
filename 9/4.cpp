#include <iostream>
#include <cstdio>
#include <vector>
#include <string>
#include <memory.h>
#include <cstdlib>
#include <algorithm>
#include <map>
using namespace std;

bool check(string s) 
{
    unsigned int i = 0;
    int n = 0, k = 0;
    while (i < s.size() && s[i] == 'a') {
        ++i;
    }
    n = i;
    if (n == 0) {
        return false;
    }
    while (i < s.size() && s[i] == '0') {
        ++i;
    }
    k = i - n;
    if (k == 0) {
        return false;
    }
    while (i < s.size() && s[i] == 'b') {
        ++i;
        --n;
    }
    if (n != 0) {
        return false;
    }
    while (i < s.size() && s[i] == '1') {
        ++i;
        --k;
    }
    if (k != 0 || i != s.size()) {
        return false;
    }
    return true;
}

int main()
{
    string s;
    while (cin >> s) {
        if (check(s)) {
            cout << "1\n";
        }
        else {
            cout << "0\n";
        }
    }
    return 0;
}