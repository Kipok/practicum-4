#include <iostream>
#include <cstdio>
#include <vector>
#include <string>
#include <memory.h>
#include <cstdlib>
#include <algorithm>
#include <map>
using namespace std;



int main()
{
    string alph;
    string word;
    vector <map <char, int>> au;
    vector <int> lasts;
    int n, b_s, m;
    cin >> word >> alph >> n >> b_s >> m;
    lasts.resize(m);
    for (int i = 0; i < m; i++) {
        cin >> lasts[i];
    }
    au.resize(n + 1);
    for (int i = 1; i <= n; i++) {
        for (unsigned int j = 0; j < alph.size(); j++) {
            cin >> au[i][alph[j]];
        }
    }
    int state = b_s;
    for (unsigned int i = 0; i < word.size(); i++) {
        if (au[state].count(word[i]) == 0) {
            cout << "NO\n";
            return 0;
        }
        state = au[state][word[i]];
    }
    if (find(lasts.begin(), lasts.end(), state) != lasts.end()) {
        cout << "YES\n";
    }
    else {
        cout << "NO\n";
    }
    return 0;
}