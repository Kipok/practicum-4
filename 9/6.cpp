#include <iostream>
#include <cstdio>
#include <vector>
#include <string>
#include <memory.h>
#include <cstdlib>
#include <algorithm>
#include <map>
using namespace std;


int main()
{
    map <string, vector <pair <char, string>>> a;
    map <string, bool> lasts;
    string s1, s2;
    char c;
    cin >> s1;
    while (s1 != "END") {
        cin >> c >> s2;
        a[s1].push_back(make_pair(c, s2));
        cin >> s1;
    }
    cin >> s1;
    while (s1 != "END") {
        lasts[s1] = true;
        cin >> s1;
    }
    cin >> s1 >> s2; // s2 - word
    for (unsigned int i = 0; i < s2.size(); i++) {
        int sign = 0;
        for (auto it = a[s1].begin(); it != a[s1].end(); ++it) {
            if (it->first == s2[i]) {
                s1 = it->second;
                sign = 1;
                break;
            }
        }
        if (!sign) {
            cout << "0\n" << i << endl << s1 << endl;
            return 0;
        }
    }
    if (lasts.count(s1)) {
        cout << "1\n" << s2.size() << endl << s1 << endl;
    }
    else {
        cout << "0\n" << s2.size() << endl << s1 << endl;
    }
    return 0;
}