#include <iostream>
#include <cstdio>
#include <string>
using namespace std;

class BinaryNumber
{
    string num;
public:
    BinaryNumber(const string &s);
    operator string () const;
    const BinaryNumber& operator++();
};

BinaryNumber::BinaryNumber(const string &s) : num(s) {}

BinaryNumber::operator string() const
{
    return num;
}

const BinaryNumber& BinaryNumber::operator++()
{
    int i = num.size() - 1;
    ++num[i];
    while (num[i] == '2') {
        --i;
        num[i + 1] = '0';
        if (i < 0) {
            break;
        }
        ++num[i];
    }
    if (i < 0) {
        num.insert(num.begin(), '1');
    }
    return *this;
}