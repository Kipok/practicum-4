#include <iostream>
#include <cstdio>
#include <string>
using namespace std;

class Avg
{
    double n;
    double sum, sum2;
public:
    Avg() : n( 0 ), sum( 0 ), sum2( 0 ) {}
    const Avg& operator +=( double x );
    friend ostream& operator <<( ostream& out, const Avg &a);
};

const Avg& Avg::operator +=( double x )
{
    ++n;
    sum += x;
    sum2 += x * x;
    return *this;
}

ostream& operator <<( ostream &out, const Avg &a )
{
    out << a.sum / a.n << endl << (a.sum2 / a.n - a.sum * a.sum / (a.n * a.n)) * a.n / (a.n - 1) << endl;
    return out;
}