#include <iostream>
#include <cstdio>
#include <string>
#include <sstream>
using namespace std;

class Exp
{
    string s;
    mutable int num;
    mutable int isCalced;
public:
    Exp(const char *s_);
    Exp & operator =(const Exp &a);
    int calc() const;
};

Exp::Exp(const char * s_) : s(string(s_)), num(0), isCalced(0) {}

int Exp::calc() const
{
    if (isCalced) {
        return num;
    }
    stringstream ss;
    ss << s;
    long long cur_num;
    char k = '+';
    while (ss >> cur_num) {
        if (k == '+') {
            num += cur_num;
        } else {
            num -= cur_num;
        }
        ss >> k;
    }
    isCalced = 1;
    return num;
}