#include <iostream>
#include <string>
#include <vector>
using namespace std;

class StringStack
{
    vector <string> st;
public:
    StringStack(const string &s);
    StringStack() {};
    StringStack & operator =(const string &s);
    StringStack & operator ,(const string &s);
    StringStack & operator +=(const string &s);
    void push(const string &s);
    string pop();
    friend ostream & operator <<(ostream &out, const StringStack &a);
};

StringStack::StringStack( const string &s )
{
    st.push_back(s);
}

StringStack & StringStack::operator =( const string &s )
{
    st.clear();
    st.push_back(s);
    return *this;
}

StringStack & StringStack::operator ,( const string &s )
{
    st.push_back(s);
    return *this;
}

StringStack & StringStack::operator +=( const string &s )
{
    st.push_back(s);
    return *this;
}

void StringStack::push(const string &s)
{
    st.push_back(s);
}

string StringStack::pop()
{
    string s = st[st.size() - 1];
    st.pop_back();
    return s;
}

ostream & operator <<(ostream &out, const StringStack &a)
{
    int d = a.st.size() - 1;
    for (int i = d; i >= 0; i--) {
        out << a.st[i] << endl;
    }
    return out;
}