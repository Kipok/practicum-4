#include <iostream>
#include <cstdio>
#include <cstdlib>
#include <string>
#include <fstream>
using namespace std;

class File 
{
    FILE *f;
    File(const File &a);
    File & operator =(const File &a);
public:
    File(string s);
    ~File(void);
    void read(char *buf, const int num) const;
    void write(const char *buf, const int num) const;
};

File::File(string s) : f(nullptr)
{
    f = fopen(s.c_str(), "r+");
}

File::~File(void)
{
    fclose(f);
}

void File::read(char *buf, const int num) const
{
    fread(buf, sizeof(char), num, this->f);
}

void File::write(const char *buf, const int num) const
{
    fwrite(buf, sizeof(char), num, this->f);
}