#include <iostream>
#include <cstdio>
#include <cstdlib>
#include <string>
using namespace std;

long long gcd(long long a, long long b) 
{
    if (!b) {
        return a;
    }
    return gcd(b, a % b);
}

long long lcd(int a, int b)
{
    return 1ll * a * b / gcd(a, b);
}

class Rational
{
    int a, b;
public:
    Rational(int a_ = 0, int b_ = 1) : a(a_), b(b_) {
        a /= gcd(abs(a_), abs(b_));
        b /= gcd(abs(a_), abs(b_));
    }
    
    Rational & Add(const Rational &x);
    Rational & Substract(const Rational &x);
    Rational & Multiply(const Rational &x);
    Rational & Divide(const Rational &x);
    bool EqualTo(const Rational &x) const;
    int CompareTo(const Rational &x) const;
    bool IsInteger() const;
    string ToString() const;
};

Rational & Rational::Add(const Rational &x)
{
    long long l = lcd(b, x.b);
    long long ca = a * (l / b), cxa = x.a * (l / x.b);
    ca += cxa;
    a = ca / gcd(abs(ca), l);
    b = l / gcd(abs(ca), l);
    return *this;
}

Rational & Rational::Substract(const Rational &x)
{
    long long l = lcd(b, x.b);
    long long ca = a * (l / b), cxa = x.a * (l / x.b);
    ca -= cxa;
    a = ca / gcd(abs(ca), l);
    b = l / gcd(abs(ca), l);
    return *this;
}

Rational & Rational::Multiply(const Rational &x)
{
    long long ca = a * x.a;
    long long cb = b * x.b;
    a = ca / gcd(abs(ca), abs(cb));
    b = cb / gcd(abs(ca), abs(cb));
    return *this;
}

Rational & Rational::Divide(const Rational &x)
{
    long long ca = a * x.b;
    long long cb = b * x.a;
    a = ca / gcd(abs(ca), abs(cb));
    b = cb / gcd(abs(ca), abs(cb));
    return *this;
}

bool Rational::EqualTo(const Rational &x) const
{
    return a == x.a && b == x.b;
}

int Rational::CompareTo(const Rational &x) const
{
    long long l = lcd(b, x.b);
    long long ca = a * (l / b), cxa = x.a * (l / x.b);
    if (ca > cxa) {
        return 1;
    }
    if (ca == cxa) {
        return 0;
    }
    return -1;
}

bool Rational::IsInteger() const 
{
    return b == 1;
}

string Rational::ToString() const
{
    return string(to_string(a) + string(":") + to_string(b));
}